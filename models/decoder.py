# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
import torch.nn as nn

def get_decoder(configuration):
    """The main model gets its decoder using this function. 
    It returns the right decoder configuration

    Args:
        configuration (dict): Parameters to chose the decoder design

    Raises:
        UnknownModelException: Raised when no design corresponds to the configuration

    Returns:
        nn.Module: The required decoder class
    """
    if configuration["model"]["decoder"] == "FCDecoder":
        return FCDecoder(configuration)
    elif configuration["model"]["decoder"] == "ImplicitDecoder":
        return ImplicitDecoder(configuration)

    else:
        raise UnknownModelException("Unknown decoder type")


class FCDecoder(nn.Module):
    """Fully connected decoder. Succession of 5 linear layers."""

    def __init__(self, configuration):
        super(FCDecoder, self).__init__()

        red = 2 if configuration["model"]["pyramidal"] else 1

        self.nof = configuration["model"]["number_of_frames"]
        self.pose_dim = configuration["model"]["number_of_joints"] * \
            configuration["model"]["joint_repr"]["dim"]

        self.translation_dim = 3
        self.timestamp_dim = 1
        self.dmpl_dim = configuration["model"]["num_dmpls"]

        output_dim = (self.pose_dim+self.translation_dim +
                      self.timestamp_dim+self.dmpl_dim)*self.nof

        hidden_dim = configuration["model"]["decoder_hidden_dim"]
        latent_dim = configuration["model"]["latent_dim"]
        condition_dim = configuration["model"]["latent_shape_dim"]

        self.decoder = nn.Sequential(
            nn.Linear(latent_dim+condition_dim, hidden_dim//red**3),
            nn.Linear(hidden_dim//red**3, hidden_dim//red**2),
            nn.ReLU(),
            nn.Linear(hidden_dim//red**2, hidden_dim//red),
            nn.Linear(hidden_dim//red, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, output_dim)
        )

    def forward(self, latent_vec, condition, timestamps=None):

        X = torch.cat((latent_vec, condition), dim=-1)
        x1 = self.decoder(X)

        poses = x1[:, 0:self.nof *
                   self.pose_dim].reshape(-1, self.nof, self.pose_dim)
        trans = x1[:, self.nof *
                   self.pose_dim:self.nof *
                   (self.pose_dim+self.translation_dim)].reshape(-1, self.nof, self.translation_dim)
        timestamp = x1[:, self.nof * (self.pose_dim+self.translation_dim):self.nof*(
            self.pose_dim+self.translation_dim+self.timestamp_dim)].reshape(-1, self.nof, self.timestamp_dim)

        if(self.dmpl_dim > 0):
            dmpls = x1[:, self.nof * (self.pose_dim+self.translation_dim +
                                      self.timestamp_dim):].reshape(-1, self.nof, self.dmpl_dim)
        else:
            dmpls = None

        return {"poses": poses, "trans": trans, "timestamp": timestamp, "dmpls": dmpls}


class FCDecoder(nn.Module):
    """Fully connected decoder. Succession of 5 linear layers."""

    def __init__(self, configuration):
        super(FCDecoder, self).__init__()

        red = 2 if configuration["model"]["pyramidal"] else 1

        self.nof = configuration["model"]["number_of_frames"]
        self.pose_dim = configuration["model"]["number_of_joints"] * \
            configuration["model"]["joint_repr"]["dim"]

        self.translation_dim = 3
        self.timestamp_dim = 1
        self.dmpl_dim = configuration["model"]["num_dmpls"]

        output_dim = (self.pose_dim+self.translation_dim +
                      self.timestamp_dim+self.dmpl_dim)*self.nof

        hidden_dim = configuration["model"]["decoder_hidden_dim"]
        latent_dim = configuration["model"]["latent_dim"]
        condition_dim = configuration["model"]["latent_shape_dim"]

        self.decoder = nn.Sequential(
            nn.Linear(latent_dim+condition_dim, hidden_dim//red**3),
            nn.Linear(hidden_dim//red**3, hidden_dim//red**2),
            nn.ReLU(),
            nn.Linear(hidden_dim//red**2, hidden_dim//red),
            nn.Linear(hidden_dim//red, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, output_dim)
        )

    def forward(self, latent_vec, condition, timestamps=None):

        X = torch.cat((latent_vec, condition), dim=-1)

        x1 = self.decoder(X)

        poses = x1[:, 0:self.nof *
                   self.pose_dim].reshape(-1, self.nof, self.pose_dim)
        trans = x1[:, self.nof *
                   self.pose_dim:self.nof *
                   (self.pose_dim+self.translation_dim)].reshape(-1, self.nof, self.translation_dim)
        timestamp = x1[:, self.nof * (self.pose_dim+self.translation_dim):self.nof*(
            self.pose_dim+self.translation_dim+self.timestamp_dim)].reshape(-1, self.nof, self.timestamp_dim)

        if(self.dmpl_dim > 0):
            dmpls = x1[:, self.nof * (self.pose_dim+self.translation_dim +
                                      self.timestamp_dim):].reshape(-1, self.nof, self.dmpl_dim)
        else:
            dmpls = None

        return {"poses": poses, "trans": trans, "timestamp": timestamp, "dmpls": dmpls}


class ImplicitDecoder(nn.Module):
    """Fully connected decoder. Succession of 5 linear layers."""

    def __init__(self, configuration):
        super(ImplicitDecoder, self).__init__()

        self.pose_dim = configuration["model"]["number_of_joints"] * \
            configuration["model"]["joint_repr"]["dim"]

        self.translation_dim = 3

        self.dmpl_dim = configuration["model"]["num_dmpls"]

        output_dim = (self.pose_dim+self.translation_dim +
                      self.dmpl_dim)

        hidden_dim = configuration["model"]["decoder_hidden_dim"]
        latent_dim = configuration["model"]["latent_dim"]
        condition_dim = configuration["model"]["latent_shape_dim"]

        self.decoder = nn.Sequential(
            nn.Linear(latent_dim+condition_dim+1, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, output_dim),
            nn.ReLU(),
            nn.Linear(output_dim, output_dim),
        )

    def forward(self, latent_vec, condition, timestamps=None):
        # timestamps BS x N x 1
        X = torch.cat((latent_vec, condition), dim=-1)

        rep_X = X[:, None, :].repeat(1, timestamps.shape[1], 1)
        rep_X = torch.cat((rep_X, timestamps), dim=-1)
        rep_X = self.decoder(rep_X)

        poses = rep_X[:, :, 0:self.pose_dim]
        trans = rep_X[:, :, self.pose_dim:self.pose_dim+self.translation_dim]

        if(self.dmpl_dim > 0):
            dmpls = rep_X[:, :, self.pose_dim + self.translation_dim:]
        else:
            dmpls = None

        return {"poses": poses, "trans": trans, "timestamp": timestamps, "dmpls": dmpls}
