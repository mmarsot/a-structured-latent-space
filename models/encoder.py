# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
import torch.nn as nn


def get_encoder(configuration):
    """The main model gets its encoder using this function.
    It returns the right encoder configuration

    Args:
        configuration (dict): Parameters to chose the encoder design

    Raises:
        UnknownModelException: Raised when no design corresponds to the configuration

    Returns:
        nn.Module: The required encoder class
    """
    if configuration["model"]["encoder"] == "FCEncoder":
        return FCEncoder(configuration)
    elif configuration["model"]["encoder"] == "FeatEncoder":
        return FeatEncoder(configuration)
    elif configuration["model"]["encoder"] == "TransformerEncoder":
        return TransformerEncoder(configuration)
    else:
        raise UnknownModelException("Unknown encoder type")


class FCEncoder(nn.Module):
    """Fully connected encoder, 4 common linear layers.
    And 2 separated linear layers for the mean and logvar vector
    """

    def __init__(self, configuration):
        super(FCEncoder, self).__init__()

        red = 2 if configuration["model"]["pyramidal"] else 1

        self.nof = configuration["model"]["number_of_frames"]
        self.pose_dim = configuration["model"]["number_of_joints"] * \
            configuration["model"]["joint_repr"]["dim"]

        self.translation_dim = 3
        self.timestamp_dim = 1

        input_dim = (self.pose_dim+self.translation_dim +
                     self.timestamp_dim)*self.nof

        hidden_dim = configuration["model"]["decoder_hidden_dim"]
        latent_dim = configuration["model"]["latent_dim"]

        self.encoder = nn.Sequential(
            nn.Flatten(),
            nn.Linear(input_dim, hidden_dim),
            nn.Linear(hidden_dim, hidden_dim//red),
            nn.ReLU(),
            nn.Linear(hidden_dim//red, hidden_dim//red**2),
            nn.Linear(hidden_dim//red**2, hidden_dim//red**3),
            nn.ReLU()
        )
        self.enc_mean = nn.Linear(hidden_dim//red**3, latent_dim)
        self.enc_logvar = nn.Linear(hidden_dim//red**3, latent_dim)

    def forward(self, data):

        X = torch.cat((data["poses"], data["trans"],
                       data["timestamp"]), dim=-1)

        X = self.encoder(X)
        return self.enc_mean(X), self.enc_logvar(X)


class FeatEncoder(nn.Module):

    def __init__(self, configuration):
        super(FeatEncoder, self).__init__()

        red = 2 if configuration["model"]["pyramidal"] else 1

        self.nof = configuration["model"]["number_of_frames"]
        self.pose_dim = configuration["model"]["number_of_joints"] * \
            configuration["model"]["joint_repr"]["dim"]

        self.translation_dim = 3
        self.timestamp_dim = 1

        input_dim = (self.pose_dim+self.translation_dim +
                     self.timestamp_dim)*self.nof

        hidden_dim = configuration["model"]["decoder_hidden_dim"]
        latent_dim = configuration["model"]["latent_dim"]

        self.enc_pose = nn.Sequential(
            nn.Flatten(),
            nn.Linear(self.pose_dim*self.nof, 160*(hidden_dim//256))
        )
        self.enc_trans = nn.Sequential(
            nn.Flatten(),
            nn.Linear(self.translation_dim*self.nof, 64*(hidden_dim//256))
        )
        self.enc_t = nn.Sequential(
            nn.Flatten(),
            nn.Linear(self.timestamp_dim*self.nof, 32*(hidden_dim//256))
        )

        self.encoder = nn.Sequential(
            nn.Linear(hidden_dim, hidden_dim//red),
            nn.ReLU(),
            nn.Linear(hidden_dim//red, hidden_dim//red**2),
            nn.Linear(hidden_dim//red**2, hidden_dim//red**3),
            nn.ReLU()
        )

        self.enc_mean = nn.Linear(hidden_dim//red**3, latent_dim)
        self.enc_logvar = nn.Linear(hidden_dim//red**3, latent_dim)

    def forward(self, data):

        f_pose = self.enc_pose(data["poses"])
        f_trans = self.enc_trans(data["trans"])
        f_t = self.enc_t(data["timestamp"])

        X = torch.cat((f_pose, f_trans, f_t), 1)

        X = self.encoder(X)
        return self.enc_mean(X), self.enc_logvar(X)


class TransformerEncoder(nn.Module):

    def __init__(self, configuration):
        super(TransformerEncoder, self).__init__()

        self.nof = configuration["model"]["number_of_frames"]
        self.pose_dim = configuration["model"]["number_of_joints"] * \
            configuration["model"]["joint_repr"]["dim"]

        self.translation_dim = 3
        self.timestamp_dim = 1

        self.d_model = self.pose_dim+self.translation_dim + self.timestamp_dim

        nhead = 4

        hidden_dim = configuration["model"]["decoder_hidden_dim"]
        latent_dim = configuration["model"]["latent_dim"]

        self.proj_layer = nn.Linear(self.d_model, latent_dim)

        encoder_layer = nn.TransformerEncoderLayer(
            d_model=latent_dim, nhead=nhead)

        self.transformer = nn.TransformerEncoder(
            encoder_layer, num_layers=4, norm=nn.LayerNorm(latent_dim))

        self.enc_mean = nn.Linear(latent_dim, latent_dim)
        self.enc_logvar = nn.Linear(latent_dim, latent_dim)

    def forward(self, data):

        X = torch.cat((data["poses"], data["trans"],
                       data["timestamp"]), dim=-1)

        X = self.proj_layer(X)

        X = torch.cat((torch.zeros(
            (X.shape[0], 1, X.shape[2]), device=X.device, dtype=X.dtype), X), dim=1)

        X = self.transformer(X.transpose(0, 1)).transpose(0, 1)
        token = X[:, 0, :]

        return self.enc_mean(token), self.enc_logvar(token)
