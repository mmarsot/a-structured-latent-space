import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from models.encoder import get_encoder
from models.decoder import get_decoder

import losses.losses as losses

from utils.preprocess import align
from utils.representation import convertPose
import utils.representation as arepr

import os


class CVAE(nn.Module):
    """Main model composed of an encoder and a decoder
    Builds a regularized latent representation of the data
    """

    def __init__(self, configuration, body_model, data_normalizer):
        """

        Args:
            configuration (dict): The configuration parameters
            body_model : The body model used to convert SMPL data to mesh data
            data_normalizer : A custom normalizer class used to normalize input data
        """
        super(CVAE, self).__init__()
        self.encoder = get_encoder(configuration)
        self.decoder = get_decoder(configuration)
        self.latent_shape_dim = configuration["model"]["latent_shape_dim"]
        self.latent_motion_dim = configuration["model"]["latent_dim"]
        self.joint_repr = configuration["model"]["joint_repr"]
        self.number_of_joints = configuration["model"]["number_of_joints"]
        self.joints = configuration["model"]["joints"]
        self.num_dmpls = configuration["model"]["num_dmpls"]
        self.latent_vec = None
        self.body_model = body_model
        self.normalizer = data_normalizer

        self.is_train = configuration["training"]['is_train']

        self.device = configuration["device"]

        if self.is_train:  # only defined during training time

            self.pre_train = configuration["training"]['pre_train']
            self.reg_weight = configuration["training"]['kl_weight']

            self.reg_loss_fn = configuration["training"]["reg_loss_fn"]

            self.optimizer = torch.optim.Adam(
                self.parameters(), lr=configuration["training"]['lr'])

            # Adaptative loss to balance different reconstruction losses see GradNorm paper for details
            # GradNorm: Gradient Normalization for Adaptive Loss Balancing in Deep Multitask Networks, Zhao Chen et al
            # https://arxiv.org/abs/1711.02257
            self.adaptative_loss = configuration["training"]["adaptative_loss"]
            self.contact_loss = False#configuration["training"]["contact_loss"]
            if self.adaptative_loss:
                self.weight_lr = configuration["training"]['weight_lr']
                self.init_losses = None
                self.inv_learning_rates = None

            if(self.pre_train):
                self.set_pre_train()
            else:
                self.set_fine_tune()

    def forward(self):
        """Run a forward pass on the input in self.in_dict, call after self.set_input()
        """
        self.mu, self.logvar = self.encoder(self.in_dict)

        self.z = self.reparameterize(self.mu, self.logvar)

        self.out_dict = self.decoder(
            self.z, self.in_dict["betas"][..., :self.latent_shape_dim], timestamps=self.in_dict["timestamp"])
        self.out_dict["betas"] = self.in_dict["betas"]

        if(not self.pre_train):
            self.out_dict["mesh_vertices"] = self.body_model(
                self.normalizer.unnormalize(self.out_dict), angle_repr=self.joint_repr, joints=self.joints)
        else:
            self.out_dict["mesh_vertices"] = None

    def backward(self):
        """Backward pass, compute the losses, call after a forward pass
        """
        self.unweighted_rec_losses = self.rec_loss_fn(
            self.in_dict, self.out_dict)

        if(self.num_dmpls == 0 and self.pre_train):
            self.unweighted_rec_losses = self.unweighted_rec_losses[:-1]

        self.weighted_rec_losses = [
            l*w for (l, w) in zip(self.unweighted_rec_losses, self.weights)]

        self.regularization_loss = self.reg_loss_fn(
            self.mu, self.logvar, gamma=self.reg_weight)

        self.loss = self.regularization_loss
        for i in range(len(self.weighted_rec_losses)):
            self.loss += self.weighted_rec_losses[i]

        if self.adaptative_loss and self.init_losses is None:
            self.init_losses = torch.tensor(
                [l.clone().detach() for l in self.weighted_rec_losses], device=self.device)+1e-5

        if(self.contact_loss):
            pass
    def set_input(self, data, data_joint_repr=arepr.AA, normalize=True, preprocessed=False):
        """Preprocess the data and set it as input for the forward pass. /!\ Can modify data dimension

        Args:
            data (dict): SMPL data
            data_joint_repr (dict, optional): The data joint representation. Defaults to AA.
            normalize (bool, optional): If True, the data is normalized using self.normalizer. Defaults to True.
            preprocessed (bool, optional): Whether or not to preprocess the data, can be done beforehand to accelerate training. Defaults to False.
        """
        if(len(data["poses"].shape) == 2):
            data["poses"] = data["poses"][None, ...]
            data["trans"] = data["trans"][None, ...]
            data["dmpls"] = data["dmpls"][None, ...]
            data["betas"] = data["betas"][None, ...]
            data["timestamp"] = data["timestamp"][None, ...]

        data["betas"] = data["betas"][...,
                                      :self.body_model.num_betas]

        if not preprocessed:
            data = align(data, input_repr=data_joint_repr)

            data["poses"] = convertPose(
                data["poses"], data_joint_repr, self.joint_repr)

        data["poses"] = data["poses"].reshape(
            data["poses"].shape[0], data["poses"].shape[1], - 1, self.joint_repr["dim"])

        data["poses"] = data["poses"][:, :, self.joints, :]
        data["poses"] = data["poses"].reshape(
            data["poses"].shape[0], data["poses"].shape[1], -1)

        if(not self.pre_train):
            data["mesh_vertices"] = self.body_model(
                data, angle_repr=self.joint_repr, use_hands=False, joints=self.joints).detach().requires_grad_(True)
        else:
            data["mesh_vertices"] = None
   
        if(normalize):
            self.in_dict = self.normalizer.normalize(data)
        else:
            self.in_dict = data
        
        for k in ["trans", "poses", "betas", "dmpls", "timestamp"]:
            self.in_dict[k] = self.in_dict[k].detach().requires_grad_(True)

    def reparameterize(self, mu, logvar):

        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        if(not self.is_train):
            return mu
        return mu + eps*std

    def set_pre_train(self):
        """
        Update internal variables to the pre training phase on SMPL parameters
        """
        self.pre_train = True
        self.rec_loss_fn = losses.smpl_rec_loss

        if(self.num_dmpls == 0):
            self.weights = torch.tensor(
                [1.0, 1.0, 1.0], requires_grad=True, device=self.device)
        else:
            self.weights = torch.tensor(
                [1.0, 1.0, 1.0, 1.0], requires_grad=True, device=self.device)

        self.unweighted_rec_losses = [torch.zeros(
            1), torch.zeros(1), torch.zeros(1), torch.zeros(1)]
        self.regularization_loss = torch.zeros(1)
        if self.adaptative_loss:
            self.weight_optimizer = torch.optim.Adam(
                [self.weights], lr=self.weight_lr)

    def set_fine_tune(self):
        """
        Update internal variables to the fine tuning phase on meshes
        """
        self.pre_train = False
        self.rec_loss_fn = losses.mesh_rec_loss
        self.weights = torch.tensor(
            [1.0, 1.0], requires_grad=True, device=self.device)
        self.unweighted_rec_losses = [torch.zeros(
            1), torch.zeros(1)]
        self.regularization_loss = torch.zeros(1)
        if self.adaptative_loss:
            self.weight_optimizer = torch.optim.Adam(
                [self.weights], lr=self.weight_lr)

    def optimize_parameters(self):
        """
        Calculate gradients and update network weights.
        """

        if(self.adaptative_loss):

            param = list(self.decoder.parameters())[-2]

            G = [torch.autograd.grad(l, param, retain_graph=True, create_graph=True, allow_unused=True)[
                0] for l in self.weighted_rec_losses]
            for i, g in enumerate(G):
                if(g is None):
                    G[i] = torch.tensor(0).to(self.device)
                else:
                    G[i] = torch.norm(g, 2)

            # G_avg = torch.mean(torch.tensor(G)).detach()

            self.inv_learning_rates = (torch.tensor(
                self.unweighted_rec_losses).to(self.device) / self.init_losses)
            self.inv_learning_rates = self.inv_learning_rates / \
                torch.mean(self.inv_learning_rates)

            # Calculating the constant target for Eq. 2 in the GradNorm paper
            C = torch.pow(self.inv_learning_rates, 1).detach()

            Lgrad = sum([F.l1_loss(G[i], C[i]) for i in range(len(G))])

            # calculating weight grad while graph is still here
            weights_grad = torch.autograd.grad(Lgrad, self.weights)[0]

        # optimizing network and dumping the graph
        self.optimizer.zero_grad()
        self.loss.backward()
        torch.nn.utils.clip_grad_norm_(self.parameters(), 1)
        self.optimizer.step()

        if(self.adaptative_loss):
            # optimizing weights
            self.weight_optimizer.zero_grad()
            self.weights.grad = weights_grad
            self.weight_optimizer.step()
            coef = self.weights.shape[0]/torch.sum(self.weights)
            # negative weights will cause the associated error to be maximized
            self.weights.data = torch.max(torch.zeros(
                len(self.weights), device=self.device)+1e-5, (self.weights+1e-5)*coef)

    def eval(self):
        self.encoder.eval()
        self.decoder.eval()
        self.is_train = False

    def train(self):
        self.encoder.train()
        self.decoder.train()
        self.is_train = True

    def test(self):
        with torch.no_grad():
            self.forward()

    def get_current_losses(self):
        """Return traning losses / errors. train.py will print out these errors on console"""

        if(self.pre_train):
            if(self.num_dmpls == 0):
                return {"pose": self.unweighted_rec_losses[0].detach().cpu(),
                        "trans": self.unweighted_rec_losses[1].detach().cpu(),
                        "time": self.unweighted_rec_losses[2].detach().cpu(),
                        "dmpls": torch.tensor(0, dtype=self.unweighted_rec_losses[0].dtype).cpu(),
                        "reg": self.regularization_loss.detach().cpu()}
            else:
                return {"pose": self.unweighted_rec_losses[0].detach().cpu(),
                        "trans": self.unweighted_rec_losses[1].detach().cpu(),
                        "time": self.unweighted_rec_losses[2].detach().cpu(),
                        "dmpls": self.unweighted_rec_losses[3].detach().cpu(),
                        "reg": self.regularization_loss.detach().cpu()}
        else:
            return {"mesh": self.unweighted_rec_losses[0].detach().cpu(),
                    "time": self.unweighted_rec_losses[1].detach().cpu(),
                    "reg": self.regularization_loss.detach().cpu()}

    def load_checkpoint(self, folder):
        """Load a checkpoint, WARNING : Not working well with GradNorm

        Args:
            folder (str): The path of the folder containing the chackpoint files

        Returns:
            int: The loaded checkpoint epoch number
        """
        self.load_state_dict(torch.load(
            os.path.join(folder, "model_check.pt")))
        self.optimizer.load_state_dict(torch.load(
            os.path.join(folder, "opti_check.pt")))

        if(self.adaptative_loss):
            self.weight_optimizer.load_state_dict(
                torch.load(os.path.join(folder, "wopti_check.pt")))
            self.weights = torch.load(os.path.join(folder, "weights_check.pt"))
            self.init_losses = torch.load(
                os.path.join(folder, "initlosses_check.pt"))

        return torch.load(os.path.join(folder, "epoch.pt")).item()

    def save_checkpoint(self, folder, epoch):
        """Save a checkpoint in a given folder

        Args:
            folder (str): the destination folder for the checkpoint files
            epoch (int): the epoch to save
        """
        self.save_networks(os.path.join(folder, "model_check.pt"))
        self.save_optimizers(os.path.join(
            folder, "opti_check.pt"), os.path.join(folder, "wopti_check.pt"))
        if(self.adaptative_loss):
            torch.save(self.weights, os.path.join(folder, "weights_check.pt"))
            torch.save(self.init_losses,
                       os.path.join(folder, "initlosses_check.pt"))

        torch.save(torch.tensor(epoch), os.path.join(
            folder, "epoch.pt"))

    def save_networks(self, out_file):
        torch.save(self.state_dict(), out_file)

    def save_optimizers(self, out_file, out_file_weights=""):

        torch.save(self.optimizer.state_dict(), out_file)
        if self.adaptative_loss:
            torch.save(self.weight_optimizer.state_dict(), out_file_weights)
