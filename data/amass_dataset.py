# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

from torch.utils.data import Dataset



class AmassDataset(Dataset):

    def __init__(self, smpl_sequences, configuration, body_model=None):
        """Initialize the dataset with the given sequences

        Args:
            smpl_sequences (SMPL_Sequence): The data represented as a list of sequence object
            configuration (dict): Provides setup information, see config_template.json for more details
            body_model : A body_model to convert smpl data to mesh data
        """

        self.body_model = body_model

        # see load_config function to know what sampling_fn is used
        self.sampling_fn = configuration["dataset"]["sampling_fn"]
        self.device = configuration["device"]
        self.dtype = configuration["dtype"]
        self.in_angle_repr = configuration["dataset"]["input_joint_repr"]

        self.smpl_dataset = [s.cast(self.dtype).to(
            self.device) for s in smpl_sequences]

    def __len__(self):
        return len(self.smpl_dataset)

    def __getitem__(self, idx):
        """Access the dataset. Frame sampling is performed there

        Args:
            idx (int): idx of the element in the dataset

        Returns:
            dict: dict containing the smpl information and the timestamps
        """

        smpl_data = self.smpl_dataset[idx].smpl_data()

        smpl_data, timestamps, sampling = self.sampling_fn(smpl_data)

        data_dict = smpl_data

        data_dict["timestamp"] = timestamps.type(self.dtype).to(self.device)

        return data_dict
