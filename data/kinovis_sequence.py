import numpy as np
import torch
import trimesh
from human_body_prior.tools.omni_tools import apply_mesh_tranfsormations_, makepath
import copy
import os

# Vertices from the SMPL template corresponding to the MoCap markers of CHUM
DEFAULT_MARKERS = dict([("head", 336),
                        ("rshoulder", 5285),
                        ("lshoulder", 1849),
                        ("abdo", 3507),
                        ("rhip", 6550),
                        ("rknee", 4493),
                        ("rheel", 6730),
                        ("rtoes", 6740),
                        ("lhip", 1446),
                        ("lknee", 1009),
                        ("lheel", 3331),
                        ("ltoes", 3341),
                        ("relbow", 5172),
                        ("rwrist", 5424),
                        ("lelbow", 1703),
                        ("lwrist", 2244)])


def load_mocap_kino(mocap_folder):
    """Load mocap information, 
    mocap data should consisy of a talign.csv file for alignement parameters, 
    and a mocap.tsv file for the actual mocap data. 


    Args:
        mocap_folder ([type]): Folder containing talign.csv and mocap.tsv

    Returns:
        (dict,list ): a dict containing alignement params and a list where
        one element is the mocap data for one frame
    """
    alignement_file = open(os.path.join(mocap_folder, "talign.csv"), 'r')
    alignement_lines = alignement_file.readlines()
    alignement_file.close

    alignement_params = dict()
    for i, l in enumerate(alignement_lines):
        item = l.replace("\n", "").split(",")
        try:
            alignement_params[item[0]] = int(item[1])
        except:
            alignement_params[item[0]] = float(item[1])

    mocap_file = open(os.path.join(mocap_folder, "mocap.tsv"), 'r')
    mocap_lines = mocap_file.readlines()
    mocap_file.close()
    for i, l in enumerate(mocap_lines[10:]):
        mocap_lines[i+10] = list(map(float,
                                     l.replace("\n", "").split("\t")))

    return alignement_params, mocap_lines


class Kinovis_Sequence:
    """Class encapsulating a segment which represents a kinovis sequence
    """

    def __init__(self, folder,
                 dtype=torch.float32, device="cuda",
                 framerate=50, markers=DEFAULT_MARKERS, align=False):
        """Initialize a sequence

        Args:
            folder (str): path of the segment folder containing a origin_folder.txt file, 
            an init.pt file and a limits.pt file. The origin folder should contain a ply/ folder containing ply meshes model-XXXXX.ply
            load_meshes (bool, optional): Whether to lad the meshes in memory. Defaults to False.
            dtype: Defaults to torch.float32.
            device: Defaults to "cuda".

        Raises:
            KeyError: [description]
        """

        self.framerate = framerate
        self.device = device
        self.dtype = dtype
        self.limits = torch.load(os.path.join(folder, "limits.pt"))
        start, end = self.limits[0], self.limits[1]

        self.folder = folder
        with open(os.path.join(self.folder, "origin_folder.txt")) as f:
            self.origin_folder = f.readline()

        self.alignement_params, self.mocap_lines = load_mocap_kino(
            self.origin_folder)

        # marker order varies with the different aquisitions
        self.markers = self.mocap_lines[9].rstrip().split("\t")[1:]
        self.indexes = [DEFAULT_MARKERS[m] for m in self.markers]

        self.mocap_data = torch.tensor(
            self.mocap_lines[10:]).reshape(-1, len(self.markers), 3).to(device)

        self.init = torch.tensor(torch.load(os.path.join(
            folder, "init.pt")), dtype=self.dtype, device=self.device)

        self.meshes = [trimesh.load(os.path.join(
            self.origin_folder, "ply", "model-%05d.ply" % i), process=False) for i in range(start, end)]

        self.mocap_data_aligned = None
        self.aligned_meshes = None

        if(align):
            self.get_aligned_data(store=True)

    def mesh_data(self):
        """Access the meshes representing the sequence
        Returns:
            list: A list of trimesh.mesh
        """

        return self.meshes

    def duration(self):
        """Returns the duration of the sequence in seconds"""

        return float(self.number_of_frames())/self.framerate

    def number_of_frames(self):

        return self.limits[1]-self.limits[0]

    def frame_data(self, index, interpol_mode=0, unit="s", nopad=False, aligned=False):
        """Access frame data specified by index

        Args:
            index (list/int): Indexes of the desired frames
            interpol_mode (int, optional): How to interpol for unexisting timestamps. Defaults to 0.
            unit (str, optional): If s, the index will be interpreted as seconds, otherwise as frame number. Defaults to "s".
            nopad (bool, optional): If False, returns the last frame for any index greater than the sequence length. Defaults to False.

        Raises:
            NotImplementedError: Not implemented interpolation mode
            IndexError: Frame index too big and nopad is True

        Returns:
            dict: SMPL data for desired frames
        """

        if(not aligned):
            meshes = self.meshes
        else:
            meshes = self.get_aligned_data()[1]
        if(unit == "s"):
            frame_id = self.framerate * \
                torch.tensor(index, dtype=torch.float32)
            if interpol_mode == 0:
                frame_id = frame_id.int()
            else:
                raise NotImplementedError(
                    "Interpolation mode not implemented yet")
        else:
            frame_id = torch.tensor(index, dtype=torch.int32)

        if (frame_id >= self.number_of_frames()).any():
            if(nopad):
                raise IndexError("Not enough frames")

            frame_id[frame_id >= self.number_of_frames()
                     ] = int(self.number_of_frames()-1)

        data = {"meshes": [], "ind": []}
        for fid in frame_id:
            data["meshes"].append(meshes[fid])
            data["ind"].append(fid)

        return data

    def save(self, new_folder):
        """Save the sequence at the location given by new_file

        Args:
            new_folder (str): Path where the data will be written
        """

        makepath(new_folder)
        with open(os.path.join(new_folder, "origin_folder.txt"), "w+") as f:
            f.write(self.origin_folder)

        torch.save(self.limits, os.path.join(
            new_folder, "limits.pt"))
        torch.save(self.init, os.path.join(
            new_folder, "init.pt"))

    def get_data(self):
        if (self.mocap_data_aligned is not None) and (self.aligned_meshes is not None):
            return self.mocap_data_aligned, self.aligned_meshes

        start, end = self.limits[0], self.limits[1]

        mocap_data= torch.zeros(
            (2*(end-start), self.mocap_data.shape[1], self.mocap_data.shape[2]), dtype=self.dtype, device=self.device)
        for i in range(2*start, 2*end):
            mocap_data[i-2*start] = self.mocap_data[i +
                                                            self.alignement_params["offset"]]

        # aligning mocap data to simulate initial joint rotation of I
       
        mocap_data /= 1000
    

        return mocap_data, self.meshes

    def get_aligned_data(self, store=False):

        if (self.mocap_data_aligned is not None) and (self.aligned_meshes is not None):
            return self.mocap_data_aligned, self.aligned_meshes

        start, end = self.limits[0], self.limits[1]

        mocap_data_aligned = torch.zeros(
            (2*(end-start), self.mocap_data.shape[1], self.mocap_data.shape[2]), dtype=self.dtype, device=self.device)
        for i in range(2*start, 2*end):
            mocap_data_aligned[i-2*start] = self.mocap_data[i +
                                                            self.alignement_params["offset"]]

        # aligning mocap data to simulate initial joint rotation of I
        r = self.init

        mocap_data_aligned /= 1000
        mocap_data_aligned = torch.cat(
            (mocap_data_aligned, torch.ones((mocap_data_aligned.shape[0], 16, 1), dtype=self.dtype, device=self.device)), dim=2)
        mocap_data_aligned = mocap_data_aligned.reshape(-1, 4, 1)
        mocap_data_aligned = torch.bmm(
            r.repeat(mocap_data_aligned.shape[0], 1, 1), mocap_data_aligned)
        mocap_data_aligned = mocap_data_aligned.reshape(
            -1, len(self.markers), 4)[:, :, :3].to(self.device)

        aligned_meshes = copy.deepcopy(self.meshes)

        apply_mesh_tranfsormations_(aligned_meshes, r.detach().cpu())

        if(store):
            self.mocap_data_aligned = mocap_data_aligned
            self.aligned_meshes = aligned_meshes

        return mocap_data_aligned, aligned_meshes
