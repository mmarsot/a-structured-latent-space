import numpy as np
import torch
import psutil
import utils.representation as arepr


class SMPL_Sequence:
    """Class encapsulating a Npz file which represents a sequence
    """

    @staticmethod
    def from_dict(in_dict, npz_file=None, angle_repr=arepr.AA, joints=list(range(22)), pose_hand=True):
        """Load a Sequence directly from a smpl data dictionnary

        Args:
            in_dict (dict): The smpl data of the sequence
            npz_file (str, optional): A path to eventually save the sequence later on. 
            Defaults to None.
            angle_repr: The joint representation of the smpl data, defaults to AA
            joints: The subset of joints of the SMPLH model joints present in the pose data (hands not included)
            pose_hand (bool): whether or not hand pose is present in the file 
        Returns:
            [SMPL_Sequence]: The sequence object containing the data
        """

        s = SMPL_Sequence(npz_file, load_smpl=False, meshes=in_dict["mesh_vertices"],
                          check_validity=False, dtype=in_dict["poses"].dtype, device=in_dict["poses"].device)

        s.data = in_dict

        return s

    def __init__(self, npz_file, load_smpl=True, meshes=None, check_validity=False,
                 dtype=torch.float64, device="cuda", pose_hand=True, angle_repr=arepr.AA, joints=list(range(22))):
        """Initialize a sequence

        Args:
            npz_file (str): The path to the npz file where the data is located
            load_smpl (bool, optional): If True, load the data in device. Defaults to True.
            meshes (tensor, optional): Mesh information corresponding to the data. Defaults to None.
            check_validity (bool, optional): Check that the npz file contains smpl information. Defaults to False.
            dtype (optional): Data type of the sequence. Defaults to torch.float64.
            device (str, optional): Defaults to "cuda".
            pose_hand (bool, optional): If True, the hand data is in the smpl data. Defaults to True.
            angle_repr (dict, optional): Angle representation of the smpl data. Defaults to AA.

        Raises:
            KeyError: Raised when failing validity check
        """
        if(check_validity):
            with np.load(npz_file) as data:
                if("poses" not in data.keys() or "trans" not in data.keys()):
                    raise KeyError

        self.npz_file = npz_file
        self.data = None
        self.dtype = dtype
        self.device = device
        self.pose_hand = pose_hand
        self.angle_repr = angle_repr
        self.joints = joints

        # if more than 80% of ram is used don't load
        if(load_smpl and psutil.virtual_memory()[2] < 80):

            with np.load(npz_file) as data:
                data_numpy = dict(data)
                self.data = {k: torch.from_numpy(
                    data_numpy[k]).type(dtype).to(device) for k in ["poses", "trans", "mocap_framerate", "dmpls", "betas"]}

                if("mocap" in data_numpy.keys()):
                    self.data["mocap"] = torch.from_numpy(data_numpy["mocap"]).type(
                        dtype).to(device)

        self.meshes = None

    def smpl_data(self):
        """Access the smpl parameters of the sequence

        Returns:
            dict: The smpl parameters of the sequence
        """
        if(self.data is None):
            data = np.load(self.npz_file)
            data_numpy = dict(data)

            data = {k: torch.from_numpy(
                data_numpy[k]).type(self.dtype).to(self.device) for k in ["poses", "trans", "mocap_framerate", "dmpls", "betas"]}
            if("mocap" in data_numpy.keys()):
                data["mocap"] = torch.from_numpy(data_numpy["mocap"]).type(
                    self.dtype).to(self.device)
        else:
            data = self.data

        return data

    def mesh_data(self, body_model=None):
        """Access the meshes representing the sequence
        Args:
            body_model (optional): Model used to extrapolate a mesh from the smpl data. Defaults to None.

        Returns:
            tensor: A MxNx3 tensor with M the number of frames and N the number of vertices per mesh or None
        """

        if self.meshes is None:
            if(body_model is None):
                return None
            else:
               
                return body_model(self.smpl_data(),
                                  angle_repr=self.angle_repr,
                                  use_hands=self.pose_hand,
                                  joints=self.joints).type(self.dtype).to(self.device)

        return self.meshes

    def duration(self):
        """Returns the duration of the sequence in seconds"""

        data = self._load_data()
        return (data["poses"].shape[0]/data["mocap_framerate"]).item()

    def framerate(self):

        data = self._load_data()
        return data["mocap_framerate"].item()

    def number_of_frames(self):

        data = self._load_data()
        return data["poses"].shape[0]

    def _load_data(self):
        """Returns the data with minimal overhead, should not be accessed externally 
        as it does not respect device and dtype

        Returns:
            dict/npz object: The data of the sequence
        """
        if(self.data is None):
            data = np.load(self.npz_file)
        else:
            data = self.data

        return data

    def load_smpl_data(self):
        """Load the smpl data in memory"""
        self.data = self.smpl_data()

    def load_mesh_data(self, body_model):
        """Load the mesh data in memory"""
        self.meshes = self.mesh_data(body_model=body_model)

    def to(self, device):
        """Transfer the sequence to a new device

        Args:
            device (str)

        Returns:
            SMPL_sequence: the casted sequence
        """

        self.device = device
        if(self.data is not None):
            keys_d = ["poses", "trans", "mocap_framerate", "dmpls", "betas"]
            if("mocap" in self.data.keys()):
                keys_d.append("mocap")
            self.data = {k: self.data[k].to(self.device) for k in keys_d}

        if(self.meshes is not None):
            self.meshes = self.meshes.to(self.device)

        return self

    def cast(self, new_type):
        """Cast the sequence to a new type

        Args:
            new_type : the new data format

        Returns:
            SMPL_sequence: the casted sequence
        """

        self.type = new_type
        if(self.data is not None):

            keys_d = ["poses", "trans", "mocap_framerate", "dmpls", "betas"]
            if("mocap" in self.data.keys()):
                keys_d.append("mocap")

            self.data = {k: self.data[k].type(self.type) for k in keys_d}

        if(self.meshes is not None):
            self.meshes = self.meshes.type(self.type)

        return self

    def frame_data(self, index, interpol_mode=0, unit="s", nopad=False):
        """Access frame data specified by index

        Args:
            index (list/int): Indexes of the desired frames
            interpol_mode (int, optional): How to interpol for unexisting timestamps. Defaults to 0.
            unit (str, optional): If s, the index will be interpreted as seconds, otherwise as frame number. Defaults to "s".
            nopad (bool, optional): If False, returns the last frame for any index greater than the sequence length. Defaults to False.

        Raises:
            NotImplementedError: Not implemented interpolation mode
            IndexError: Frame index too big and nopas is True

        Returns:
            dict: SMPL data for desired frames
        """

        data = self.smpl_data()

        if(unit == "s"):
            frame_id = data["mocap_framerate"] * \
                torch.tensor(index, dtype=torch.float32,
                             device=data["mocap_framerate"].device)
            if interpol_mode == 0:
                frame_id = frame_id.int()
            else:
                raise NotImplementedError(
                    "Interpolation mode not implemented yet")
        else:
            frame_id = torch.tensor(index, dtype=torch.int32)

        if (frame_id >= data["poses"].shape[0]).any():
            if(nopad):
                raise IndexError("Not enough frames")
            frame_id[frame_id >= data["poses"].shape[0]] = (
                data["poses"].shape[0]-1)

        data_frame = {k: data[k][frame_id.tolist(), :] for k in [
            "poses", "trans", "dmpls"]}
        data_frame["betas"] = data["betas"]
        data_frame["ind"] = frame_id.tolist()

        return data_frame

    def save(self, new_file):
        """Save the sequence at the location given by new_file

        Args:
            new_file (str): Path where the data will be written
        """

        tmp_dev = self.device
        self.to("cpu")
        np.savez(new_file, **self._load_data())
        self.to(tmp_dev)
