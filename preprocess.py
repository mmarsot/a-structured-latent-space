# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387


import argparse
from body_models.smpl_model import SMPLH
from utils.preprocess import align
from utils.file_management import load_config
from utils.data_management import load_sequences
from utils.representation import *
import os
from human_body_prior.tools.omni_tools import makepath
import numpy as np
import glob
import tqdm
import time
def process_sequence(data_repr,in_folder,out_folder,s):
    
    data = s.smpl_data()

    data = align(
        data, input_repr=AA)
   
    data["poses"] = convertPose(
        data["poses"], AA,data_repr)

    data["poses"] = data["poses"].reshape(
        1, data["poses"].shape[1], -1, 
        data_repr["dim"])

    data["poses"] = data["poses"].reshape(1, data["poses"].shape[1], -1)

    for k in data.keys():
        data[k] = data[k].squeeze(dim=0).cpu().detach()

    makepath(s.npz_file.replace(
        in_folder,out_folder), isfile=True)
    np.savez(s.npz_file.replace(
        in_folder,out_folder), **data)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("config", nargs="?",
                        help="path to a configuration file, defaults to config.json", default="config.json")
    parser.add_argument("in_folder", nargs="?",
                        help="cropped data folder", default="croppedData/")
    parser.add_argument("out_folder", nargs="?",
                        help="output folder", default="croppedDataPreprocessed/")
    
    args = parser.parse_args()
    configuration = load_config(args.config)
    
    
    
    sequences = load_sequences(
        args.in_folder, dtype=configuration["dtype"], device=configuration["device"])


    smplh = SMPLH(configuration)

    a=time.time()
   
        
    for s in tqdm.tqdm(sequences):
   
        process_sequence(configuration["dataset"]["input_joint_repr"],
                        args.in_folder,args.out_folder,s)
       
    print("Preprocessing took %ds"%int(time.time()-a))
    exit(0)
