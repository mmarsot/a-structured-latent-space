# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
import torch.nn.functional as F


def smpl_rec_loss(input_dict, rec_dict):
    """Compute the reconstruction losses for pre training on SMPL parameters directly

    Args:
        input_dict (dict): smpl data of the input
        rec_dict (dict): smpl data of the recoonstruction

    Returns:
        list: [pose loss, translation loss, timestamp loss, dmpls loss]
    """

    loss_pose = F.mse_loss(
        rec_dict["poses"], input_dict["poses"], reduction="mean")
    loss_trans = F.mse_loss(
        rec_dict["trans"], input_dict["trans"], reduction="mean")
    loss_timestamp = F.mse_loss(
        rec_dict["timestamp"], input_dict["timestamp"], reduction="mean")

    if(rec_dict["dmpls"] is not None):
        loss_dmpls = F.mse_loss(
            rec_dict["dmpls"], input_dict["dmpls"], reduction="mean")
    else:
        loss_dmpls = torch.tensor(
            0, dtype=loss_pose.dtype, device=loss_pose.device)
    return [loss_pose, loss_trans, loss_timestamp, loss_dmpls]


def mesh_rec_loss(input_dict, rec_dict):
    """Compute the reconstruction losses for the fine tune pass. 

    Args:
        input_dict (dict): smpl data of the input with meshes
        rec_dict (dict): smpl data of the recoonstruction with meshes

    Returns:
        list: [mesh loss, timestamp loss]
    """
    loss_mesh = F.mse_loss(
        rec_dict["mesh_vertices"], input_dict["mesh_vertices"], reduction="mean")
    loss_timestamp = F.mse_loss(
        rec_dict["timestamp"], input_dict["timestamp"], reduction="mean")

    return [loss_mesh, loss_timestamp]


def kl_divergence(mu1, logvar1, gamma=0.1, red="mean"):
    """Returns kl divergence between mu and logvar and a normal distribution. 

    Args:
        mu1 (tensor): The means of the distribution
        logvar1 (tensor): The logarithm of the variance of the distribution
        gamma (float, optional): A scaling coefficient. Defaults to 0.1.

    Shape:
        mu1 : N x D with N the batch size 
        logvar1 : N x D with N the batch size

    Returns:
        tensor: The averaged KL divergence over the entire batch
    """

    mu2 = torch.zeros(1).to(mu1.device)
    logvar2 = torch.zeros(1).to(logvar1.device)
    if red == "mean":
        return torch.mean((-0.5*(1+logvar1-logvar2-((mu1-mu2).pow(2)/logvar2.exp())-(logvar1-logvar2).exp())*gamma)**2)
    elif red == "max":
        return torch.mean(torch.max((-0.5*(1+logvar1-logvar2-((mu1-mu2).pow(2)/logvar2.exp())-(logvar1-logvar2).exp())*gamma)**2, dim=-1)[0])
