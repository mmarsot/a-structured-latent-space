# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

from data.amass_dataset import AmassDataset
from body_models.smpl_model import SMPLH
from torch.utils.data import DataLoader
from utils.file_management import load_config
from utils.data_management import load_sequences
from normalizers.normalizer_smpl import SMPL_normalizer

from models.model import CVAE
import torch
import time
import os
import argparse

from human_body_prior.tools.omni_tools import makepath

from utils.model_visualizer import Visualizer

from shutil import copyfile


def average_losses(losses):

    losses_name = losses[0]["losses"].keys()
    avg_loss = {k: 0 for k in losses_name}

    bs_sum = 0
    for d in losses:

        bs = d["bs"]
        bs_sum += bs

        for k in losses_name:
            avg_loss[k] = avg_loss[k] + d["losses"][k] * bs

    for k in losses_name:
        avg_loss[k] = avg_loss[k].detach().cpu() / bs_sum

    return avg_loss


def create_dataset(configuration, split):

    smpl_sequences = load_sequences(
        configuration[split]["folder"], dtype=configuration["dtype"],
        device=configuration["device"], load=configuration["load"],
        pose_hand=configuration["dataset"]["pose_hand"],
        angle_repr=configuration["dataset"]["input_joint_repr"])

    smplh = SMPLH(configuration)
    dataset = AmassDataset(smpl_sequences, configuration, smplh)

    loader = DataLoader(
        dataset, batch_size=configuration[split]["batch_size"], shuffle=True)

    return dataset, loader


def train(configuration):

    visualizer = Visualizer(configuration)

    train_dataset, train_loader = create_dataset(configuration, "training")
    train_dataset_size = len(train_dataset)
    print('The number of training samples = {0}'.format(train_dataset_size))

    val_dataset, vald_loader = create_dataset(configuration, "validation")
    val_dataset_size = len(val_dataset)
    print('The number of validation samples = {0}'.format(val_dataset_size))

    print('Initializing model...')
    normalizer = SMPL_normalizer(configuration)
    model = CVAE(configuration, train_dataset.body_model,
                 normalizer).to(configuration["device"])
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    import numpy as np
    params = sum([np.prod(p.size()) for p in model_parameters])
    print("Number of parameters %d"%int(params))
    if(configuration["model"]["initialization"] != ""):
        model.load_state_dict(torch.load(
            configuration["model"]["initialization"]))

    try:
        starting_epoch = model.load_checkpoint(
            configuration["training"]["checkpoint_folder"])
        print("Checkpoint found, resuming training from epoch %d" %
              starting_epoch)

    except FileNotFoundError:
        print("No checkpoint found")
        starting_epoch = 0

    num_epochs = configuration['training']['max_epochs']

    makepath(configuration["experiment"]["folder"])
    out_file = configuration["experiment"]["out_file"]

    all_vald_losses = {k: [] for k in model.get_current_losses().keys()}
    all_train_losses = {k: [] for k in model.get_current_losses().keys()}

    for epoch in range(starting_epoch, num_epochs):
        train_losses, vald_losses = [], []

        epoch_start_time = time.time()  # timer for entire epoch

        model.train()

        for i, data in enumerate(train_loader):  # inner loop within one epoch

            model.set_input(data,
                            data_joint_repr=configuration["dataset"]["input_joint_repr"],
                            preprocessed=configuration["dataset"]["preprocessed"])

            # with torch.autograd.detect_anomaly():
            model.forward()
            model.backward()

            model.optimize_parameters()

            train_losses.append(
                {"losses": model.get_current_losses(), "bs": data["poses"].shape[0]})

            if (i+1) % configuration["training"]["printout_freq"] == 0:
                visualizer.print_current_losses(
                    epoch, num_epochs, i+1, len(train_loader), average_losses(train_losses))
                # print_losses(epoch, train_losses, model.pre_train, end="\r")
        visualizer.print_current_losses(
            epoch, num_epochs, i+1, len(train_loader), average_losses(train_losses))
        model.eval()
        for i, data in enumerate(vald_loader):
            model.set_input(data,
                            preprocessed=configuration["dataset"]["preprocessed"],
                            data_joint_repr=configuration["dataset"]["input_joint_repr"])
            model.test()
            vald_losses.append(
                {"losses": model.get_current_losses(), "bs": data["poses"].shape[0]})

        # print('Saving model at the end of epoch {0}'.format(epoch))

        if(model.adaptative_loss):
            print("Weights", model.weights)

        train_losses = average_losses(train_losses)
        vald_losses = average_losses(vald_losses)

        for k in all_train_losses.keys():

            all_train_losses[k].append(train_losses[k])
            all_vald_losses[k].append(vald_losses[k])

        if(model.pre_train and all_vald_losses["pose"][-1] == min(all_vald_losses["pose"])):
            model.save_networks(out_file)

        elif not model.pre_train and all_vald_losses["mesh"][-1] == min(all_vald_losses["mesh"]):
            model.save_networks(out_file)

        visualizer.print_current_losses(
            epoch, num_epochs, len(vald_loader), len(vald_loader), vald_losses)
        # visualizer.plot_current_losses(epoch, 1, train_losses)

        if(epoch % configuration["training"]["save_freq"] == 0):
            torch.save(all_train_losses, os.path.join(
                configuration["experiment"]["folder"], 'train_loss.pt'))
            torch.save(all_vald_losses, os.path.join(
                configuration["experiment"]["folder"], 'vald_loss.pt'))
            if(configuration["training"]["checkpoint_folder"] != ""):
                makepath(configuration["training"]["checkpoint_folder"])
                model.save_checkpoint(
                    configuration["training"]["checkpoint_folder"], epoch)

        print('End of epoch {} / {} \t Time Taken: {:.4f} sec'.format(epoch,
                                                                      num_epochs, time.time() - epoch_start_time))

    torch.save(all_train_losses, os.path.join(
        configuration["experiment"]["folder"], 'train_loss.pt'))
    torch.save(all_vald_losses, os.path.join(
        configuration["experiment"]["folder"], 'vald_loss.pt'))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("config", nargs="?",
                        help="path to a configuration file, defaults to config.json", default="config.json")
    parser.add_argument("model", nargs="?",
                        help="path to a model to load as initialization", default="")

    args = parser.parse_args()

    configuration = load_config(args.config)
    makepath(configuration["experiment"]["folder"])
    copyfile(args.config, os.path.join(
        configuration["experiment"]["folder"], "config.json"))

    torch.set_default_dtype(configuration["dtype"])
    train(configuration)
