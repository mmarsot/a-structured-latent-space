# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
import argparse
import numpy as np
import os
from tqdm import tqdm
import time

SPLITS = {
    "training":["ACCAD",
                "EKUT",
                "HumanEva",
                "MPI_HDM05",
                "Transitions_mocap",
                "CMU", 
                "Eyes_Japan_Dataset", 
                "KIT", 
                "MPI_Limits"],
    "validation":["MPI_mosh",
                  "SFU",
                  "TotalCapture"]
}

def process_sequence(segmentation,amass_path,out_path,sequence_name):

    amass_seq = sequence_name.replace(
            "/morpheo-nas2/mmarsot/AMASSnh/AMASStrain/","")
    amass_seq =  amass_seq.replace(
        "/morpheo-nas2/mmarsot/AMASSnh/AMASStest/","")
    
      
    
            
    for i,segment in enumerate(segmentation[sequence_name]):
        
        with np.load(os.path.join(amass_path,sequence_name)) as data:
            subset = amass_seq.split("/")[0]
            split=None
            for k in SPLITS.keys():
                if subset in SPLITS[k]:
                    split=k
            if split is None:
                return 0 
            if not os.path.exists(os.path.join(out_path,split,subset)):
                os.mkdir(os.path.join(out_path,split,subset))
             
            sequence = amass_seq.replace(subset+"/","")\
                .replace("/","_").replace("poses.npz","")
          
               
            if(not os.path.exists(os.path.join(out_path,split,subset,sequence))):
                os.mkdir(os.path.join(out_path,split,subset,sequence))
            
            cropped_data = {
                "poses":data["poses"][segment[0]:segment[1]],
                "trans":data["trans"][segment[0]:segment[1]],
                "betas":data["betas"],
                "mocap_framerate":data["mocap_framerate"] ,    
                "dmpls":data["dmpls"][segment[0]:segment[1]],
                "gender":data["gender"]
            }
            
            
            np.savez(
                os.path.join(
                    out_path,split,subset,sequence,sequence+"%d.npz"%i),
                **cropped_data)
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("amass_folder",
                        help="path to amass npz files")
    parser.add_argument("out_folder",nargs="?",
                        help="output folder path for cropped data",
                        default="croppedData")
    parser.add_argument("--mp",action='store_true',
                        help=" with multiprocessing, defaults to false")
   

    args = parser.parse_args()
    
    if args.mp:
        import multiprocessing as mp
    a=time.time()
    segmentation = torch.load("segments.pt")
    if not os.path.exists(args.out_folder):
        os.mkdir(args.out_folder)
        [os.mkdir(os.path.join(args.out_folder,split)) \
            for split in SPLITS.keys()]
    if args.mp:  
        pool = mp.Pool(mp.cpu_count())
    for sequence_name in tqdm(segmentation.keys()):
        
        if not args.mp:
            process_sequence(
                segmentation,args.amass_folder,args.out_folder,sequence_name)
            # [process_motion(out_experiment_path,in_experiment_path, motion,bm)for motion in motions]
        else:
            
            pool.apply_async(
                process_sequence, 
                args=(segmentation,args.amass_folder,args.out_folder,sequence_name)
                ) 
    if args.mp:  
        pool.close()
        pool.join()
    
    print("Segmentation done in %ds"%int(time.time()-a))
            
            