# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

from data.amass_dataset import AmassDataset
import argparse
from tqdm import tqdm
from utils.file_management import load_config
from utils.data_management import load_sequences
from utils.representation import convertPose
import os
import torch

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("config", nargs="?",
                        help="path to a configuration file, defaults to config.json", default="config.json")
    parser.add_argument("--overwrite", action="store_true",
                        help="If true, overwrites the existing file")

    args = parser.parse_args()

    configuration = load_config(args.config)

    if (not args.overwrite) and os.path.exists(configuration["normalization"]["normalization_file"]):
        exit(0)

    smpl_sequences = load_sequences(
        configuration["training"]["folder"], 
        dtype=configuration["dtype"],
        device=configuration["device"], 
        load=configuration["load"],
        pose_hand=configuration["dataset"]["pose_hand"],
        angle_repr=configuration["dataset"]["input_joint_repr"])

    output_file = configuration["normalization"]["normalization_file"]
    dataset = AmassDataset(smpl_sequences, configuration)

    n = 0

    for i, data in tqdm(enumerate(dataset)):

        pose = convertPose(
            data["poses"], 
            configuration["dataset"]["input_joint_repr"], 
            configuration["model"]["joint_repr"])

        pose = pose.reshape(
            pose.shape[0], - 1, 
            configuration["model"]["joint_repr"]["dim"])

        pose = pose[..., configuration["model"]["joints"], :]
        pose = pose.reshape(
            pose.shape[0], -1)

        if i == 0:

            if(configuration["normalization"]["only_center"]):
                trans_mean = torch.sum(data["trans"], dim=0)
                time_mean = torch.sum(data["timestamp"], dim=0)

            else:
                trans_min = torch.min(data["trans"], dim=0, keepdim=True)[0]
                trans_max = torch.max(data["trans"], dim=0, keepdim=True)[0]

                time_min = torch.min(data["timestamp"], dim=0, keepdim=True)[0]
                time_max = torch.max(data["timestamp"], dim=0, keepdim=True)[0]

            dmpls_mean = torch.sum(data["dmpls"], dim=0)
            pose_mean = torch.sum(pose, dim=0)
            n += pose.shape[0]

            betas = data["betas"][None, :]
        else:
            dmpls_mean += torch.sum(data["dmpls"], dim=0)
            pose_mean += torch.sum(pose, dim=0)
            n += pose.shape[0]
            if (configuration["normalization"]["only_center"]):
                trans_mean = torch.sum(data["trans"], dim=0)
                time_mean = torch.sum(data["timestamp"], dim=0)
            else:
                trans_min = torch.min(
                    torch.cat((data["trans"], trans_min), dim=0), dim=0, keepdim=True)[0]
                trans_max = torch.max(
                    torch.cat((data["trans"], trans_max), dim=0), dim=0, keepdim=True)[0]
                time_min = torch.min(
                    torch.cat((data["timestamp"], time_min), dim=0), dim=0, keepdim=True)[0]
                time_max = torch.max(
                    torch.cat((data["timestamp"], time_max), dim=0), dim=0, keepdim=True)[0]

            betas = torch.cat((data["betas"][None, :], betas), dim=0)

    if(configuration["normalization"]["simple_trans"] and not configuration["normalization"]["only_center"]):
        trans_min = trans_min.min()
        trans_max = trans_max.max()

    if(configuration["normalization"]["simple_pose"]):
        identity = torch.eye(
            3, dtype=configuration["dtype"], device=configuration["device"])
        identity = convertPose(
            identity, {"name": "mat", "dim": 9}, configuration["model"]["joint_repr"])
        njoint = pose_mean.shape[0] // configuration["model"]["joint_repr"]["dim"]
        identity = identity.reshape(-1).repeat(njoint)
        pose_mean = identity
    else:
        pose_mean = pose_mean / n

    dmpls_mean = dmpls_mean / n

    if(configuration["normalization"]["only_center"]):
        trans_mean = trans_mean/n
        time_mean = time_mean/n

    for i, data in tqdm(enumerate(dataset)):
        pose = convertPose(data["poses"], configuration["dataset"]
                           ["input_joint_repr"], configuration["model"]["joint_repr"])

        pose = pose.reshape(
            pose.shape[0], - 1, configuration["model"]["joint_repr"]["dim"])

        pose = pose[..., configuration["model"]["joints"], :]
        pose = pose.reshape(
            pose.shape[0], -1)

        if i == 0:
            pose_var = torch.sum((pose_mean-pose)**2, dim=0)
            dmpls_var = torch.sum((dmpls_mean-data["dmpls"])**2, dim=0)
            if(configuration["normalization"]["only_center"]):
                trans_var = torch.sum((trans_mean-data["trans"])**2, dim=0)
                time_var = torch.sum((time_mean-data["timestamp"])**2, dim=0)
        else:
            pose_var += torch.sum((pose_mean-pose)**2, dim=0)
            dmpls_var += torch.sum((dmpls_mean-data["dmpls"])**2, dim=0)
            if(configuration["normalization"]["only_center"]):
                trans_var += torch.sum((trans_mean-data["trans"])**2, dim=0)
                time_var += torch.sum((time_mean-data["timestamp"])**2, dim=0)

    if(configuration["normalization"]["simple_pose"]):
        pose_std = torch.ones(len(
            configuration["model"]["joints"])*configuration["model"]["joint_repr"]["dim"],
            dtype=configuration["dtype"],
            device=configuration["device"])
    else:
        pose_std = torch.sqrt(pose_var/(n-1))

    dmpls_std = torch.sqrt(dmpls_var/(n-1))

    if(configuration["normalization"]["only_center"]):
        trans_std = torch.sqrt(trans_var/(n-1))
        time_std = torch.sqrt(time_var/(n-1))

    betas = torch.unique(betas, dim=0)
    betas_mean = torch.mean(betas, dim=0)
    betas_std = torch.std(betas, dim=0)

    scaling_info = {
        "poses": (pose_mean, pose_std),
        "dmpls": (dmpls_mean, dmpls_std),
        "betas": (betas_mean, betas_std)
    }
    if(configuration["normalization"]["only_center"]):
        scaling_info["trans"] = (trans_mean, trans_std)
        scaling_info["timestamp"] = (time_mean, time_std)
        print(trans_mean, trans_std, time_mean, time_std)
    else:
        scaling_info["trans"] = (trans_min, trans_max)
        scaling_info["timestamp"] = (time_min, time_max)

    torch.save(scaling_info, output_file)
