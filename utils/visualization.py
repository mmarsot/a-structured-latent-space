# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import trimesh
from human_body_prior.mesh import MeshViewer
import time
import itertools
from utils.ImgStack import ImgStack
import numpy as np
from human_body_prior.tools.omni_tools import apply_mesh_tranfsormations_
from human_body_prior.tools.omni_tools import colors, makepath
from human_body_prior.mesh.sphere import points_to_spheres
import pyrender
import matplotlib
import seaborn as sns
import matplotlib.pyplot as plt
import cv2
import os
import tqdm


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


def draw_onscreen(mesh_viewer, meshes):
    mesh_viewer.viewer.render_lock.acquire()
    mesh_viewer.set_static_meshes(meshes)
    mesh_viewer.viewer.render_lock.release()


def visualize_sequence(sequence, body_model, mesh_viewer=None, fps=None, width=1200, height=1200,ground=False):
    """Visualize a sequence encapsulated in the SMPL_Sequence class

    Args:
        sequence (SMPL_Sequence): The sequence to visualize
        body_model : A body model to convert the sequence to meshes
        mesh_viewer (optional) : Visualizer for the sequence, create a new one by default. Defaults to None.
        fps (int, optional): The framerate of the visualized sequence. Defaults to None.
    """

    # create new mesh_viewer if none specified
    close = False
    if mesh_viewer is None:
        close = True
        mesh_viewer = MeshViewer(
            width=width, height=height, use_offscreen=False)

    if(fps is None):
        fps = int(sequence.smpl_data()["mocap_framerate"])

    ground_mesh =trimesh.Trimesh(vertices=np.array([[0,0,0],
                                                    [-10,-10,0],
                                                    [-10,10,0],
                                                    [10,10,0],
                                                    [10,-10,0]],dtype=np.float),
                               faces=np.array([[0,1,2],[0,2,3],[0,3,4],[0,4,1]]), process=False)
    
    
    for mesh_data in sequence.mesh_data(body_model=body_model):
        mesh = trimesh.Trimesh(vertices=mesh_data.cpu(),
                               faces=body_model.get_faces().cpu(), colors = np.tile(colors["grey"],6890),process=False)
        if(ground):
            _, intersections_faces = trimesh.intersections.mesh_plane(
                mesh,np.array([0,0,1],dtype=np.float),np.array([0,0,0],dtype=np.float),return_faces=True)
            for f in intersections_faces:
                for v in mesh.faces[f]:
                    mesh.visual.vertex_colors[v] = np.array([255.0,0.0,0.0,255.0])
            draw_onscreen(mesh_viewer, [mesh,ground_mesh])
        else:
            draw_onscreen(mesh_viewer, [mesh])
        time.sleep(1.0/fps)

    if(close):
        mesh_viewer.close_viewer()

def find_furthest(points):
    mdist = 0
    a,b = None,None
    for p1 in points:
        for p2 in points:
            dist =  np.square(p1-p2).sum()
            if(dist>mdist):
                mdist=dist
                a,b=p1,p2
    print(mdist)      
    return a,b
def visualize_meshes(vertices, faces, mesh_viewer=None, fps=50,ground=False):
    """Visualize a sequence of meshes

    Args:
        vertices : A list of 2D array or a 3axis array representing the vertex data
        faces : A list of 2D array or a single 2D array representing the faces
        mesh_viewer (optional): Visualizer for the sequence. Defaults to None.
        fps (int, optional): The framerate used during visualization. Defaults to 50.
    """
    h_adj = -0.05
    ground_mesh =trimesh.Trimesh(vertices=np.array([[0,0.0,h_adj],
                                                    [-10,-10,h_adj],
                                                    [-10,10,h_adj],
                                                    [10,10,h_adj],
                                                    [10,-10,h_adj]],dtype=np.float),
                               faces=np.array([[0,1,2],[0,2,3],[0,3,4],[0,4,1]]), process=False)
    
    close = False
      

    # rearrange in 1 frame batch if needed to allow itaeration
    if type(vertices) != list and len(vertices.shape) == 2:
        vertices = vertices.reshape(1, *vertices.shape)
    if type(faces) != list and len(faces.shape) == 2:
        faces = faces.reshape(1, *faces.shape)

    contacts = np.zeros((len(vertices),4,3),dtype=np.float32)
    for i,(mesh_vertices, mesh_faces) in enumerate(zip(vertices, itertools.cycle(faces))):
        mesh = trimesh.Trimesh(vertices=mesh_vertices,
                               faces=mesh_faces, process=False)
        
        if(ground):
            _, intersections_faces = trimesh.intersections.mesh_plane(
                mesh,np.array([0,0,1],dtype=np.float),np.array([0,0,h_adj],dtype=np.float),return_faces=True)
            # if(len(intersections_faces)>0):
                
            #     verts = mesh_vertices[mesh_faces[intersections_faces]]
            #     p1,p2 =find_furthest(verts)
            #     print(len(intersections_faces),i)
            for f in intersections_faces:
                for v in mesh.faces[f]:
                    mesh.visual.vertex_colors[v] = np.array([255.0,0.0,0.0,255.0])
            draw_onscreen(mesh_viewer, [mesh,ground_mesh])
        else:
            draw_onscreen(mesh_viewer, [mesh])
        time.sleep(1.0/fps)

    if(close):
        mesh_viewer.close_viewer()


def visualize_data_batch(data_dict, body_model, mesh_viewer=None, fps=10):

    # create new mesh_viewer if none specified

    for i in range(data_dict["poses"].shape[0]):
        sub_data_dict = {k: data_dict[k][i, ...] for k in data_dict.keys()}
        meshes = body_model(
            sub_data_dict, angle_repr={"name": "aa", "dim": 3}, use_hands=False)

        for mesh_data in meshes.cpu():
            mesh = trimesh.Trimesh(vertices=mesh_data,
                                   faces=body_model.get_faces().cpu(), process=False)
            draw_onscreen(mesh_viewer, [mesh])
            time.sleep(1.0/fps)

    mesh_viewer.close_viewer()


def get_image_from_data(mesh_data, mv, faces,
                        clrs=np.tile(colors['grey'], (6890, 1)),
                        transfo=trimesh.transformations.scale_matrix(0.6)):
    """
    Create a OpenCV RGB image from a frame. mv is a meshviewer needed for rendering the mesh
    """

    mesh = trimesh.Trimesh(vertices=mesh_data.cpu().detach(),
                           faces=faces.detach().cpu(),
                           vertex_colors=clrs, process=False)

    apply_mesh_tranfsormations_([mesh], transfo)

    origin = trimesh.creation.uv_sphere(radius=0.05)
    origin.visual.vertex_colors = [1, 0, 0]
    tfs = np.eye(4)
    tfs[0:3, 3] = np.array([0, 0, 0])
    origin = pyrender.Mesh.from_trimesh(origin, poses=tfs)

    mv.set_static_meshes([mesh, origin])
    body_image = mv.render()
    img = body_image.astype(np.uint8)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return img


def seq2vid(seq,  output_path, body_model, clrs=np.tile(colors['grey'], (6890, 1)), width=1200, height=1200):
    """ Write a video from a sequence"""

    out = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(
        *'MP4V'), seq.framerate(), (height, width))
    mv = MeshViewer(width=width, height=height, use_offscreen=True)

    mesh_data = seq.mesh_data(body_model=body_model)
    for i in tqdm.tqdm(range(seq.number_of_frames())):

        im = get_image_from_data(
            mesh_data[i], mv, body_model.get_faces(), clrs=clrs)
        out.write(im)
    out.release()


def seqs2vid(sequences, output_path, body_model, width=120, height=120, layout=[-1, 1], output_framerate=50,
             clrs=np.tile(colors['grey'], (100, 6890, 1)),
             transfos=[trimesh.transformations.scale_matrix(0.8)]):
    """Write a video from multiple sequences, can specify multiple transformations to have different point of views"""

    if(layout[0] < 0):
        layout[0] = len(sequences)//layout[1]
    elif(layout[1] < 0):
        layout[1] = len(sequences)//layout[0]

    assert layout[0]*layout[1] == len(sequences)

    istack = ImgStack(width, height, layout[1], layout[0])

    out = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(
        *'MP4V'), output_framerate, (istack.img.shape[1], istack.img.shape[0]))

    mv = MeshViewer(width=width, height=height, use_offscreen=True)

    duration = max([seq.duration() for seq in sequences])

    for n, t in tqdm.tqdm(enumerate(np.linspace(0, duration, 100))):
        istack.reset()
        for transfo in transfos:
            for i, seq in enumerate(sequences):

                if(type(clrs) == list):
                    frame_color = clrs[i][n]
                else:
                    frame_color = clrs[n]

                mesh_data = body_model(seq.frame_data(t),
                                       angle_repr=seq.angle_repr,
                                       use_hands=seq.pose_hand,
                                       joints=seq.joints).type(seq.dtype).to(seq.device)
                im = cv2.resize(
                    get_image_from_data(mesh_data, mv, body_model.get_faces(), clrs=frame_color,
                                        transfo=transfo), (width, height))
                istack.addToStack(im)

        out.write(istack.img)

    out.release()


def seq2obj(sequence, body_model, out_folder="seq/", step=10, unit="i", clrs=None, iterations=None, ext="obj"):
    """Write meshes on disk from a SMPL_Sequence object

    Args:
        sequence (SMPL_Sequence): The sequence to write on disk
        body_model : a body_model to convert SMPL data to meshes 
        out_folder (str, optional): folder where to save th meshes. Defaults to "seq/".
        step (int, optional): Save a mesh every "step" frames. Defaults to 10.
        unit (str, optional): If "i", step is a number of frames, if "s", step is in seconds. Defaults to "i".
        clrs (optional): Color information for the meshes. Defaults to None.
        iterations (optional): Overrides step, specify specific timestamps/frames to write. Defaults to None.
        ext (str, optional): File extension. Defaults to "obj".
    """
    makepath(out_folder)

    cmap = matplotlib.cm.get_cmap('Spectral')

    if(unit == "s"):
        step = int(step*sequence.framerate())
        n = sequence.duration()
        if(iterations is None):
            iterations = np.array(
                list(range(0, sequence.number_of_frames(), step))) / sequence.framerate()

    else:
        n = sequence.number_of_frames()
        if(iterations is None):
            iterations = range(sequence.number_of_frames())

    if(clrs is None):
        clrs = [np.tile(cmap(i/iterations[-1]), (6890, 1)) for i in iterations]
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    datax = []
    datay =[]
    dataz = []
    
    datax2 = []
    datay2 =[]
    dataz2 = []
    mesh_datap = []
    colors_mesh = []
    colorsp = []
    #ln, = ax.plot(mesh_data[[6859]],mesh_data[[6859]],mesh_data[[6859]], linestyle="", marker="o",ms=0.5)
            
    for k, i in enumerate(iterations):
        try:
            mesh_data = body_model(sequence.frame_data(i, unit=unit, nopad=True),
                                   angle_repr=sequence.angle_repr,
                                   use_hands=sequence.pose_hand,
                                   joints=sequence.joints).type(sequence.dtype).to(sequence.device)
        except IndexError:
            break

        mesh = trimesh.Trimesh(vertices=mesh_data.cpu().detach(),
                               faces=body_model.get_faces().cpu(),
                               vertex_colors=clrs[k], process=False)
       
        if unit == "s":
            mesh.export(os.path.join(out_folder, "frame_%f.%s" % (i, ext)))
            
            if(k==0 or k==24 or k==39):
                mesh_datap.append(mesh_data)
                colors_mesh.append(clrs[k][0])
                colors_mesh[-1][-1]=0.1
                colors_mesh[-1] = tuple(colors_mesh[-1])
                
            datax.append(mesh_data[3466,0])
            datay.append(mesh_data[3466,2])
            dataz.append(mesh_data[3466,1])
            
            datax2.append(mesh_data[6859,0])
            datay2.append(mesh_data[6859,2])
            dataz2.append(mesh_data[6859,1])
            colorsp.append(tuple(clrs[k][0]))
            print(clrs[k][0])
            
            
            
            for name,cts in zip(("tg","td","pg","pd"),(3466,6859,3261,6653)):
                makepath(os.path.join(out_folder, name))
               # sm_cts.vertices = mesh_data[[cts]].cpu().detach()
                sm_cts = trimesh.creation.uv_sphere(radius=0.02)
                sm_cts.visual.vertex_colors = np.tile(clrs[k][0:1],(sm_cts.vertices.shape[0],1))
                sm_cts.vertices = mesh_data[[cts]].cpu().numpy()+sm_cts.vertices
                sm_cts.export(os.path.join(out_folder, name,"contact_%f.%s" % (i, "ply")))
        else:
            mesh.export(os.path.join(out_folder, "frame_%d.%s" % (i, ext)))
   