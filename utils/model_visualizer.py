class Visualizer():
    """This class includes several functions that can display images and print logging information.
    """

    def __init__(self, configuration):
        """Initialize the Visualizer class.
        Input params:
            configuration -- stores all the configurations
        """
        self.configuration = configuration  # cache the option
        self.display_id = 0
        self.name = configuration["experiment"]['name']

        self.ncols = 0
        

    def reset(self):
        """Reset the visualization.
        """
        pass

    def print_current_losses(self, epoch, max_epochs, it, max_iters, losses):
        """Print current losses on console.
        Input params:
            epoch: Current epoch.
            max_epochs: Maximum number of epochs.
            iter: Iteration in epoch.
            max_iters: Number of iterations in epoch.
            losses: Training losses stored in the format of (name, float) pairs
        """
        message = '[epoch: {}/{}\t, iter: {}/{} \t] '.format(
            epoch, max_epochs, it, max_iters)
        for k, v in losses.items():
            message += '{0}: {1:.6f} '.format(k, v)

        print(message)  # print the message
