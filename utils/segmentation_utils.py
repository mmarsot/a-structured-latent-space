# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import numpy as np
import os
from human_body_prior.tools.omni_tools import makepath


def write_segments(sequence, segments, sub_folder, output_folder, seq_mocap=None):

    for i, s in enumerate(segments):

        with np.load(sequence.npz_file) as data2:
            data = dict()

            data["dmpls"] = data2["dmpls"][s[0]:s[1], :]
            data["poses"] = data2["poses"][s[0]:s[1], :]
            data["trans"] = data2["trans"][s[0]:s[1], :]
            data["segment"] = np.array([s[0], s[1]])
            data["mocap_framerate"] = data2["mocap_framerate"]
            data["gender"] = data2["gender"]
            data["betas"] = data2["betas"] 
         
            folder = os.path.join(output_folder, sub_folder)
           
            makepath(os.path.join(folder, sequence.npz_file.replace('.npz', '').split(os.sep)[-2], 
                sequence.npz_file.replace('.npz', '').split(os.sep)[-1]))
            np.savez(os.path.join(folder, sequence.npz_file.replace('.npz', '').split(os.sep)[-2], "%s/segment_%d.npz" % (
                sequence.npz_file.replace('.npz', '').split(os.sep)[-1], i)), **data)

    
def remove_overlapping_segments(minimas, dist_matrix):

    non_overlapping = []
    for m1 in minimas:
        add = True
        l1 = (m1[1] - m1[0])
        if(l1 < 0):
            add = False

        else:
            for m2 in minimas:
                l2 = (m2[1] - m2[0])

                score1 = dist_matrix[m1[1], m1[0]]
                score2 = dist_matrix[m2[1], m2[0]]

                # S1 inter S2
                d = min(m1[1], m2[1]) - max(m1[0], m2[0])

                # If S1 inter S2 greater than 0
                if(d >= 0):
                    relative_overlap = d / min(l1, l2)
                    if(relative_overlap > 0.8 and score1 < score2):
                        add = False

        if(add):
            non_overlapping.append(m1)

    return np.array(non_overlapping)


def remove_overlapping_segments_multiple(minimas):

    non_overlapping = []
    for m1, score1 in minimas:
        # print(score1)
        add = True
        l1 = (m1[1] - m1[0])
        if(l1 < 0):
            add = False

        else:
            for m2, score2 in minimas:
                l2 = (m2[1] - m2[0])

                # score1 = dist_matrix[m1[1],m1[0]]
                # score2 = dist_matrix[m2[1],m2[0]]

                # S1 inter S2
                d = min(m1[1], m2[1]) - max(m1[0], m2[0])

                # If S1 inter S2 greater than 0
                if(d >= 0):
                    relative_overlap = d / min(l1, l2)
                    if(relative_overlap > 0.8 and score1 < score2):
                        add = False

        if(add):
            non_overlapping.append(m1)

    return np.array(non_overlapping)


def compute_acc_matrix(matrix):

    acc_matrix = np.ones(matrix.shape, dtype=np.float64)*1e6

    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if(i == 0 and j == 0):
                acc_matrix[i, j] = matrix[i, j]
            elif(i == 0):
                acc_matrix[i, j] = matrix[i, j] + acc_matrix[i, j-1]
            elif(j == 0):
                acc_matrix[i, j] = matrix[i, j] + acc_matrix[i-1, j]
            else:
                acc_matrix[i, j] = matrix[i, j] + \
                    min(acc_matrix[i-1, j], acc_matrix[i, j-1])

    return acc_matrix


def dtwdist(X, Y, i=0, j=-1, acc_cost=None):

    M = X.shape[0]

    SEG = Y[i:j, :]

    if(acc_cost is None):
        d, cost_matrix, acc_cost_matrix, path = accelerated_dtw(
            X, SEG, dist=lambda x, y: np.sum(np.square(x - y)))
    else:
        acc_cost_matrix = acc_cost[:, i:j]

    return (acc_cost_matrix[-1, -1]-acc_cost[0, i]) / (acc_cost_matrix.shape[0] + acc_cost_matrix.shape[1])
