# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import numpy as np
import torch
import os
import re
import json

from utils.sampling import *
from losses.losses import *


def load_config(path):
    """Load the configuration file, this function add fields to the json file

    Args:
        path (srt): Path of the json config file

    Returns:
        dict: The experiment configuration
    """
    configuration = json.load(open(path))

    if(configuration["dataset"]["sampling"] == "uniform"):
        configuration["dataset"]["sampling_fn"] = lambda data: uniform_sampling(
            data, n=configuration["model"]["number_of_frames"])
    elif(configuration["dataset"]["sampling"] == "random"):
        configuration["dataset"]["sampling_fn"] = lambda data: random_sampling(
            data, n=configuration["model"]["number_of_frames"])
    else:
        configuration["dataset"]["sampling_fn"] = identity_sampling

    if configuration["type"] == "float":
        configuration["dtype"] = torch.float32
    elif configuration["type"] == "double":
        configuration["dtype"] = torch.float64
    else:
        configuration["dtype"] = torch.float32

    if not ('pose_hand' in configuration["dataset"].keys()):

        print("old config")
        configuration["dataset"]["pose_hand"] = not configuration["dataset"]["preprocessed"]

    if(configuration["dataset"]["preprocessed"]):
        configuration["dataset"]["joints"] = configuration["model"]["joints"]
    else:
        configuration["dataset"]["joints"] = list(range(22))

    try:
        n = configuration["normalization"]["only_center"]
    except:
        print("old config, default to standard normalization")
        configuration["normalization"]["only_center"] = False
    try:
        if(configuration["training"]["reg_loss"] == "classic"):
            configuration["training"]["reg_loss_fn"] = kl_divergence
        elif configuration["training"]["reg_loss"] == "max":
            configuration["training"]["reg_loss_fn"] = lambda x, y, gamma=0.1: kl_divergence(
                x, y, gamma=gamma, red="max")
    except:
        print("old config, default to classic reg loss")
        configuration["training"]["reg_loss_fn"] = kl_divergence
    return configuration


def write_config(configuration, path):
    """Write a configuration to a path

    Args:
        configuration (dict]): The configuration to save
        path (str): The path where to save it
    """

    del configuration["dataset"]["sampling_fn"]
    del configuration["dtype"]
    del configuration["training"]["reg_loss_fn"]
    json.dump(configuration, open(path, "w+"))


def list_files_structured(folder):
    """List all filenames in a folder recursively,
    keep the arborescence of the folder

    Args:
        folder (str): Path to the desired folder

    Returns:
        dict: Recursive dictionnary where each key is a folder, each folder is represented by a dictionnary
        and has a "files" key if there is any files in it
    """
    filenames = dict()

    for r, d, f in os.walk(folder):
        position = r.split(os.sep)
        current = filenames
        for p in position:
            try:
                current = current[p]
            except:
                current[p] = dict()
                current = current[p]

        current["files"] = f

    return filenames


def list_files(folder, pattern=""):
    """List all files from a given folder

    Args:
        folder (str): Path to the desired folder
        pattern (str, optional): A pattern that is looked for in the filenames. Defaults to "".

    Returns:
        list: A list of strings, paths to every file from the folder
    """
    files = []
    for r, d, f in os.walk(folder):
        for file in f:
            if re.search(pattern, file) is not None:
                files.append(os.path.join(r, file))

    return files


def list_subfolders(folder, pattern=""):
    """List all files from a given folder

    Args:
        folder (str): Path to the desired folder
        pattern (str, optional): A pattern that is looked for in the filenames. Defaults to "".

    Returns:
        list: A list of strings, paths to every folder from the folder
    """
    folders = []
    for r, d, f in os.walk(folder):
        for folder in d:
            if re.search(pattern, folder) is not None:
                folders.append(os.path.join(r, folder))

    return folders
