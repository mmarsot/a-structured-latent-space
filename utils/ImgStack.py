# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import cv2
import numpy as np


class StackFullException(Exception):
    """Raise when trying to add to a full stack"""
    pass


class ImgStack:
    """
    ImgStack is encapsulating an image. 
    It stacks stack_width*stack_height images (of the same dimensions) in a rectangle 
    """

    def __init__(self, width, height, stack_height, stack_width):

        self.img = np.zeros(
            (height*stack_height, width*stack_width, 3), np.uint8)

        self.stack_width = stack_width
        self.stack_height = stack_height

        self.width = width
        self.height = height

        self.count = 0

    def addToStack(self, img):

        if self.count == self.stack_height*self.stack_width:
            raise StackFullException

        i = self.count // self.stack_width
        j = self.count % self.stack_width

        self.img[i*self.height:(i+1)*self.height, j *
                 self.width:(j+1)*self.width] = img
        self.count += 1

    def reset(self):

        self.count = 0
        self.img = np.zeros((self.height*self.stack_height,
                             self.width*self.stack_width, 3), np.uint8)
