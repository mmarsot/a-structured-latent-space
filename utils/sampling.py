# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
import random


def identity_sampling(smpl_data):
    fps = smpl_data["mocap_framerate"]
    nof = smpl_data["poses"].shape[0]
    return smpl_data, torch.linspace(0, nof / fps, nof)[:, None], list(range(nof))


def uniform_sampling(smpl_data, n=100):

    fps = smpl_data["mocap_framerate"]
    nof = smpl_data["poses"].shape[0]
    samples = (torch.arange(0, n, device=fps.device) * nof/n).long()
    sampled_data = dict()
    
    sampled_data["poses"] = smpl_data["poses"][samples, :]
    sampled_data["trans"] = smpl_data["trans"][samples, :]
    sampled_data["dmpls"] = smpl_data["dmpls"][samples, :]
    sampled_data["betas"] = smpl_data["betas"]
    sampled_data["mocap_framerate"] = smpl_data["mocap_framerate"]
    if("mocap" in smpl_data.keys()):
        sampled_data["mocap"] = smpl_data["mocap"][samples, ...]

    return sampled_data, samples[:, None].float()/fps, samples


def random_sampling(smpl_data, n=100):

    fps = smpl_data["mocap_framerate"]
    nof = smpl_data["poses"].shape[0]

    start = random.randint(0, nof-n if nof > n else 0)
    end = start+n if start+n < nof else nof

    samples = torch.arange(start, end, step=(
        end-start)/n, device=fps.device).long()
    sampled_data = dict()
    sampled_data["poses"] = smpl_data["poses"][samples, :]
    sampled_data["trans"] = smpl_data["trans"][samples, :]
    sampled_data["dmpls"] = smpl_data["dmpls"][samples, :]
    sampled_data["betas"] = smpl_data["betas"]
    sampled_data["mocap_framerate"] = smpl_data["mocap_framerate"]

    return sampled_data, samples[:, None].float()/fps, samples


def rnn_sampling(smpl_data, max_frames=300):

    nof = smpl_data["poses"].shape[0]

    diff = max_frames - nof

    device = smpl_data["poses"].device
    dtype = smpl_data["poses"].dtype

    sampled_data = dict()
    sampled_data["poses"] = torch.cat(
        (smpl_data["poses"], torch.zeros((diff, smpl_data["poses"].shape[1]), device=device, dtype=dtype)), dim=0)
    sampled_data["trans"] = torch.cat(
        (smpl_data["trans"], torch.zeros((diff, smpl_data["trans"].shape[1]), device=device, dtype=dtype)), dim=0)
    sampled_data["dmpls"] = torch.cat(
        (smpl_data["dmpls"], torch.zeros((diff, smpl_data["dmpls"].shape[1]), device=device, dtype=dtype)), dim=0)
    sampled_data["betas"] = smpl_data["betas"]
    sampled_data["mocap_framerate"] = smpl_data["mocap_framerate"]
    sampled_data["nof"] = torch.tensor(nof, device=device, dtype=torch.int64)

    # for rnn, no timestamps, so we save the number of frames for each sequence as it varies
    timestamp = torch.tensor(
        range(nof), device=device, dtype=dtype)
    timestamp = torch.cat(
        (timestamp, timestamp[-1]*torch.ones(diff, device=device, dtype=dtype)))

    return sampled_data, timestamp/smpl_data["mocap_framerate"], None
