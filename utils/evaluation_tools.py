# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
from data.smpl_sequence import SMPL_Sequence
from pytorch3d.loss import chamfer_distance
from pytorch3d.loss.chamfer import _handle_pointcloud_input
from pytorch3d.structures import Meshes, Pointclouds
from pytorch3d.ops.knn import knn_gather, knn_points
import numpy as np
from scipy import linalg
from utils.representation import convertPose
from utils.visualization import visualize_sequence


def compute_errors(ref_seq, test_seqs, angular=False, body_model=None, nsteps=100, timesteps=None):
    """Compute error vertex per vertex (in m) between a ref sequence and a list of sequences

    Args:
        ref_seq (SMPL_Sequence): The reference sequence
        test_seqs ([SMPL_Sequence]): A list of SMPL_Sequence
        angular (bool, optional): If true, computes error in joint domain, 
        /!\ ref_seq and test_seqs should then have same joint repr. Defaults to False.
        body_model ([type], optional): [description]. Defaults to None.
        nsteps (int, optional): Number of samples for the comparison, irrelevant if timesteps is specified. Defaults to 100.
        timesteps (tensor, optional): The timestamps at which we want to compare. Defaults to None.

    Raises:
        Exception: If angular is False, specify a body model

    Returns:
        list, list: The list of error tensors and the list of duration errors
    """
    if(not angular and body_model is None):
        raise Exception("If angular is False, specify a body model")

    errors = []
    duration_errors = []

    if(timesteps is None):
        timesteps = torch.linspace(0, ref_seq.duration(), nsteps)

    frames_ref = ref_seq.frame_data(timesteps, unit="s")

    meshes_ref = body_model(
        frames_ref, angle_repr=ref_seq.angle_repr, joints=ref_seq.joints, use_hands=ref_seq.pose_hand)

    for seq in test_seqs:
        frames = seq.frame_data(timesteps, unit="s")

        meshes = body_model(frames, angle_repr=seq.angle_repr,
                            joints=seq.joints, use_hands=seq.pose_hand)

        if(angular):
            frames["poses"] = convertPose(
                frames["poses"], seq.angle_repr, ref_seq.angle_repr)
            errors.append(torch.sqrt(torch.pow(
                frames["poses"]-frames_ref["poses"], 2)))

        else:

            errors.append(torch.sqrt(
                torch.sum(torch.pow((meshes-meshes_ref), 2), dim=-1)))
            errors[-1][timesteps > ref_seq.duration()] = 0

        duration_errors.append(abs(seq.duration()-ref_seq.duration()))

    return errors, duration_errors


def compute_chamfer(kino_frames, rec_frames):

    errors = []

    frames = kino_frames
    meshes_kino = [m for m in frames]

    errors.append(torch.zeros(len(rec_frames), 6890))
    for i, (mk, p) in enumerate(zip(meshes_kino, rec_frames)):

        x, x_lengths, x_normals = _handle_pointcloud_input(
            p[None, :, :], None, None)
        y, y_lengths, y_normals = _handle_pointcloud_input(
            mk[None, :, :], None, None)

        N, P1, D = x.shape
        P2 = y.shape[1]

        cham_norm_x = x.new_zeros(())
        cham_norm_y = x.new_zeros(())

        x_nn = knn_points(x, y, lengths1=x_lengths,
                          lengths2=y_lengths, K=1)
        y_nn = knn_points(y, x, lengths1=y_lengths,
                          lengths2=x_lengths, K=1)

        cham_x = x_nn.dists[..., 0]  # (N, P1)
        cham_y = y_nn.dists[..., 0]  # (N, P2)

        errors[-1][i] = cham_x.squeeze()

    return errors


def compute_errors_kino(kino_seq, amass_seqs, body_model, nsteps=100):
    """Compute errors between a dense scan sequence and a SMPL_Sequence,
    error is the Chamfer distance in m

    Args:
        kino_seq (Kinovis_Sequence)): The reference dense sequence
        amass_seqs ([type]): The SMPL sequence obtained with our model
        body_model ([type]): A body model to convert smpl data to mesh data
        nsteps (int, optional): Number of samples for the comparison. Defaults to 100.

    Returns:
        [type]: [description]
    """
    errors = []
    duration_errors = []
    timesteps = torch.linspace(0, kino_seq.duration(), nsteps).tolist()

    frames_ref = kino_seq.frame_data(timesteps, unit="s", aligned=True)
    meshes_kino = [torch.tensor(m.vertices, dtype=torch.float32, device=amass_seqs[0].device)
                   for m in frames_ref["meshes"]]

    for seq in amass_seqs:
        frames = seq.frame_data(timesteps, unit="s")
        meshes_amass = body_model(frames)

        errors.append(torch.zeros(len(meshes_amass), 6890))
        for i, (mk, ma) in enumerate(zip(meshes_kino, meshes_amass)):

            x, x_lengths, x_normals = _handle_pointcloud_input(
                ma[None, :, :], None, None)
            y, y_lengths, y_normals = _handle_pointcloud_input(
                mk[None, :, :], None, None)

            N, P1, D = x.shape
            P2 = y.shape[1]

            cham_norm_x = x.new_zeros(())
            cham_norm_y = x.new_zeros(())

            x_nn = knn_points(x, y, lengths1=x_lengths,
                              lengths2=y_lengths, K=1)
            y_nn = knn_points(y, x, lengths1=y_lengths,
                              lengths2=x_lengths, K=1)

            cham_x = x_nn.dists[..., 0]  # (N, P1)
            cham_y = y_nn.dists[..., 0]  # (N, P2)
            errors[-1][i]=(torch.mean(torch.sqrt(cham_x[cham_x != 0]).squeeze())+torch.mean(torch.sqrt(cham_y[cham_y != 0]).squeeze()))*0.5
            # errors[-1][i] = cham_x.squeeze()

    duration_errors.append(abs(seq.duration()-kino_seq.duration()))

    return errors, duration_errors


def poulaine(smpl_sequence, body_model):

    smpl_data = smpl_sequence.smpl_data()

    X = body_model(smpl_data,
                   angle_repr=smpl_sequence.angle_repr,
                   use_hands=smpl_sequence.pose_hand,
                   joints=smpl_sequence.joints,
                   all_info=True)

    joints = X.Jtr

    root = joints[:, 0, :].clone()
    left_hip = joints[:, 1, :].clone()
    right_hip = joints[:, 2, :].clone()

    left_ankle = joints[:, 7, :].clone()
    right_ankle = joints[:, 8, :].clone()

    axisX = (right_hip-left_hip)
    axisX = axisX / axisX.norm(dim=-1, keepdim=True)
    axisY = root - (right_hip+left_hip)/2
    axisY = axisY / axisY.norm(dim=-1, keepdim=True)

    axisZ = torch.zeros(axisX.shape, device=axisX.device)
    axisZ[:, 0] = axisX[:, 1]*axisY[:, 2] - axisX[:, 2]*axisY[:, 1]
    axisZ[:, 1] = axisX[:, 2]*axisY[:, 0] - axisX[:, 0]*axisY[:, 2]
    axisZ[:, 2] = axisX[:, 0]*axisY[:, 1] - axisX[:, 1]*axisY[:, 0]
    axisZ = axisZ / axisZ.norm(dim=-1, keepdim=True)

    axisX = axisX.reshape(-1, 3, 1)
    axisY = axisY.reshape(-1, 3, 1)
    axisZ = axisZ.reshape(-1, 3, 1)
    P = torch.cat((axisZ, axisX, axisY), dim=-1)
    Pinv = torch.inverse(P)

    Oprim = -((right_hip+left_hip)/2).reshape(-1, 3, 1)

    left_ankle = torch.bmm(Pinv, left_ankle.reshape(-1, 3, 1)+Oprim).squeeze()
    right_ankle = torch.bmm(
        Pinv, right_ankle.reshape(-1, 3, 1)+Oprim).squeeze()
    return left_ankle.detach().cpu(), right_ankle.detach().cpu()


def calculate_activation_statistics(activations):
    activations = activations.cpu().numpy()
    mu = np.mean(activations, axis=0)
    sigma = np.cov(activations, rowvar=False)
    return mu, sigma


def calculate_frechet_distance(mu1, sigma1, mu2, sigma2, eps=1e-6):
    """Numpy implementation of the Frechet Distance.
    The Frechet distance between two multivariate Gaussians X_1 ~ N(mu_1, C_1)
    and X_2 ~ N(mu_2, C_2) is
            d^2 = ||mu_1 - mu_2||^2 + Tr(C_1 + C_2 - 2*sqrt(C_1*C_2)).
    Stable version by Dougal J. Sutherland.
    Params:
    -- mu1   : Numpy array containing the activations of a layer of the
               inception net (like returned by the function 'get_predictions')
               for generated samples.
    -- mu2   : The sample mean over activations, precalculated on an
               representative data set.
    -- sigma1: The covariance matrix over activations for generated samples.
    -- sigma2: The covariance matrix over activations, precalculated on an
               representative data set.
    Returns:
    --   : The Frechet Distance.
    """

    mu1 = np.atleast_1d(mu1)
    mu2 = np.atleast_1d(mu2)

    sigma1 = np.atleast_2d(sigma1)
    sigma2 = np.atleast_2d(sigma2)

    assert mu1.shape == mu2.shape, \
        'Training and test mean vectors have different lengths'
    assert sigma1.shape == sigma2.shape, \
        'Training and test covariances have different dimensions'

    diff = mu1 - mu2

    # Product might be almost singular
    covmean, _ = linalg.sqrtm(sigma1.dot(sigma2), disp=False)
    if not np.isfinite(covmean).all():
        msg = ('fid calculation produces singular product; '
               'adding %s to diagonal of cov estimates') % eps
        print(msg)
        offset = np.eye(sigma1.shape[0]) * eps
        covmean = linalg.sqrtm((sigma1 + offset).dot(sigma2 + offset))

    # Numerical error might give slight imaginary component
    if np.iscomplexobj(covmean):
        if not np.allclose(np.diagonal(covmean).imag, 0, atol=1e-3):
            m = np.max(np.abs(covmean.imag))
            raise ValueError('Imaginary component {}'.format(m))
        covmean = covmean.real

    tr_covmean = np.trace(covmean)

    return (diff.dot(diff) + np.trace(sigma1) + np.trace(sigma2) - 2 * tr_covmean)


def compute_size(betas, body_model):

    data = dict()
    data["poses"] = torch.zeros((betas.shape[0], 1, 22*3)).to(betas.device)
    data["trans"] = torch.zeros((betas.shape[0], 1, 3)).to(betas.device)
    data["betas"] = betas

    data["dmpls"] = None
    mesh = body_model(data, angle_repr={"name": "aa", "dim": 3}, joints=range(
        22), use_hands=False)

    return (mesh[..., 411, :]-mesh[..., 6826, :]).norm(dim=-1)
