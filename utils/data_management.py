# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
import numpy as np
from data.smpl_sequence import SMPL_Sequence

from utils.representation import convertPose
from utils.file_management import list_files
import tqdm
import sys

import copy


def load_sequences(folder, condition_fn=lambda s: True,
                   dtype=torch.float64, device="cuda", load=True, pose_hand=True,
                   angle_repr={"name": "aa", "dim": 3},
                   joints=list(range(22)),
                   with_tqdm=True,
                   mp=False):
    """Load sequences from a file path

    Args:
        folder (str): folder containing the npz sequences
        condition_fn (function, optional): A condition function toselect the sequence. Defaults to lambda s:True.
        dtype (optional): The data type of the sequences. Defaults to torch.float64.
        device (str, optional): The pytorch device for the sequences. Defaults to "cuda".
        load (bool, optional): Whether to load or not the data on the device. Defaults to True.
        pose_hand (bool, optional): Whether or nto the data includes SMPL hand pose. Defaults to True.
        angle_repr (dict, optional): Angle representation of the input data. Defaults to {"name": "aa", "dim": 3}.
        with_tqdm (bool, optional): Activate tqdm or not. Defaults to True.

    Returns:
        list: The sequences as SMPL_Sequence objects
    """

    smpl_files = list_files(folder, ".npz")

    smpl_sequences = []
    ignored_files = []
    if(with_tqdm):
        wrapper = tqdm.tqdm
    else:
        def wrapper(x, file=None): return x

    
    for f in wrapper(smpl_files, file=sys.stdout):
        if('shape.npz' in f):
            ignored_files.append(f)
            continue

        s = SMPL_Sequence(f, load_smpl=load, dtype=dtype,
                          device=device, pose_hand=pose_hand, angle_repr=angle_repr, joints=joints)

        if(condition_fn(s)):
            smpl_sequences.append(s)
        else:
            ignored_files.append(f)
            continue

    return smpl_sequences


def smpl_seq2input(seq, configuration):
    """Transform a sequence in a dictionnary usable by the CVAE model

    Args:
        seq (SMPL_Sequence): The sequence to convert
        configuration ([type]): The configutation parameters needed for the
        sampling, dtype and device

    Returns:
        dict: The SMPL data in a dict format
    """

    smpl_data = seq.to(configuration["device"]).cast(
        configuration["dtype"]).smpl_data()

    smpl_data, timestamps, sampling = configuration["dataset"]["sampling_fn"](
        smpl_data)

    data_dict = smpl_data
    data_dict["timestamp"] = timestamps.type(
        configuration["dtype"]).to(configuration["device"])

    return data_dict


def out2smpl_seq(out_dict, framerate=50, gender="neutral", out_file=None,
                 in_repr={"name": "six", "dim": 6}, out_repr={"name": "aa", "dim": 3},
                 joints=list(range(22)),
                 strict=False):
    """Convert a network output dictionnary to a SMPL_Sequence object

    Args:
        out_dict (dict): The newtwork output to convert
        framerate (int, optional): The desired framerate for the output. Defaults to 50.
        gender (str, optional): The desired gender for the output. Defaults to "neutral".
        out_file ([type], optional): [description]. Defaults to None.
        in_repr (dict, optional): [description]. Defaults to {"name": "six", "dim": 6}.
        out_repr (dict, optional): [description]. Defaults to {"name": "aa", "dim": 3}.
        joints ([type], optional): [description]. Defaults to list(range(22)).

    Raises:
        ValueError: [description]

    Returns:
        [type]: [description]
    """

    duration = out_dict["timestamp"][0, -1]

    if((out_dict["timestamp"] < 0).any()):

        if(strict):
            raise ValueError("Sequence has negative timestamp")
        else:
            # print("At least a timestamp is negative, Min value: %f, Rounding up to 0" %
            #       out_dict["timestamp"].min())
            out_dict["timestamp"][out_dict["timestamp"] < 0] = 0

    indices = list(range(0, int(duration*framerate)))
    number_of_frames = len(indices)

    if(out_dict["dmpls"] is None):
        keys = ["poses", "trans"]
    else:
        keys = ["poses", "trans", "dmpls"]

    data = {k: torch.zeros(
        (number_of_frames, out_dict[k].shape[-1]), dtype=torch.float64, device=out_dict["poses"].device, requires_grad=False) for k in keys}

    data["betas"] = out_dict["betas"].squeeze().detach()

    data["mocap_framerate"] = torch.tensor(
        framerate, dtype=torch.float64, requires_grad=False)
    data["gender"] = np.array(gender)

    dict_indices = (out_dict["timestamp"]*framerate).int().reshape(-1).tolist()

    for i in indices:

        for k in keys:
            if i in dict_indices:
                data[k][i, :] = out_dict[k][0, dict_indices.index(i), :]
            else:
                data[k][i, :] = data[k][i-1, :]

    data["poses"] = convertPose(data["poses"], in_repr, out_repr)

    poses = torch.zeros(
        (data["poses"].shape[0], 52*out_repr["dim"]), device=data["poses"].device)

    for i, j in enumerate(joints):

        poses[:, j*out_repr["dim"]:(j+1)*out_repr["dim"]] = data["poses"][:,
                                                                          i*out_repr["dim"]:(i+1)*out_repr["dim"]]

    data["poses"] = poses

    data["mesh_vertices"] = out_dict["mesh_vertices"]

    if(out_dict["dmpls"] is None):
        data["dmpls"] = torch.zeros((number_of_frames, 8), requires_grad=False)

    for k in keys:
        data[k] = data[k].detach()

    return SMPL_Sequence.from_dict(data, out_file, angle_repr=out_repr, joints=joints, pose_hand=False)


def convert_framerate(seq, target=30):
    """Convert the framerate of a SMPL sequence to a target framerate
        performs a copy of the sequence

    Args:
        seq (SMPL_Sequence): The sequence we wish to modify
        target (int, optional): [description]. Defaults to 30.

    Returns:
        SMPL_Sequence: new sequence with target framerate
    """
    coef = seq.framerate()/target

    if(coef < 1):
        # if we need more frame, lineraly inteprolate the missing frames
        # /!\ the interpolation of pose is not regularized when the angle repr is not 6D
        new_seq = copy.deepcopy(seq)
        new_seq.data["poses"] = torch.zeros(
            (int(target*seq.duration()), new_seq.data["poses"].shape[1]), device=seq.device, dtype=seq.dtype)
        new_seq.data["trans"] = torch.zeros(
            (int(target*seq.duration()), new_seq.data["trans"].shape[1]), device=seq.device, dtype=seq.dtype)
        new_seq.data["dmpls"] = torch.zeros(
            (int(target*seq.duration()), new_seq.data["dmpls"].shape[1]), device=seq.device, dtype=seq.dtype)
        new_seq.data["mocap_framerate"] = torch.tensor([target])

        for i, x in enumerate(np.linspace(0, seq.duration(), target*seq.duration())):
            fr1 = int(x*seq.framerate())
            t1 = fr1 / seq.framerate()

            fr2 = fr1 + 1
            t2 = fr2 / seq.framerate()

            if(fr1 >= seq.number_of_frames() or fr2 >= seq.number_of_frames()):
                new_seq.data["poses"][i] = seq.data["poses"][-1]
                new_seq.data["trans"][i] = seq.data["trans"][-1]
                new_seq.data["dmpls"][i] = seq.data["dmpls"][-1]

            else:
                new_seq.data["poses"][i] = (
                    x-t1)/(t2-t1) * seq.data["poses"][fr2] + (t2-x) / (t2-t1) * seq.data["poses"][fr1]
                new_seq.data["trans"][i] = (
                    x-t1)/(t2-t1) * seq.data["trans"][fr2] + (t2-x) / (t2-t1) * seq.data["trans"][fr1]
                new_seq.data["dmpls"][i] = (
                    x-t1)/(t2-t1) * seq.data["dmpls"][fr2] + (t2-x) / (t2-t1) * seq.data["dmpls"][fr1]
        return new_seq

    else:
        # if the desired framerate is smaller than the actual one,
        # it just subsamples pose, trans and dmpls
        new_seq = copy.deepcopy(seq)

        new_seq.data["poses"] = new_seq.data["poses"][[
            int(i*coef) for i in range(int(seq.number_of_frames()/coef))], :]

        new_seq.data["trans"] = new_seq.data["trans"][[
            int(i*coef) for i in range(int(seq.number_of_frames()/coef))], :]

        new_seq.data["dmpls"] = new_seq.data["dmpls"][[
            int(i*coef) for i in range(int(seq.number_of_frames()/coef))], :]
        new_seq.data["mocap_framerate"] = torch.tensor([target])

    return new_seq
