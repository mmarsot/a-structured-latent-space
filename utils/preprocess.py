# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch
from utils.representation import convertPose
import utils.representation as arepr


def align(data, input_repr=arepr.AA):
    """Spatially align the data

    Args:
        data (dict): Input data to align
        input_repr (optional): THe data joint representation. Defaults to AA.

    Returns:
        dict: aligned data, add a batch dimension if no batch, 
        so squeezing may be needed afterwards to remove it
    """

    poses = convertPose(data["poses"], input_repr, arepr.MAT)
   
    if(len(poses.shape) == 2):
        batch_size = 1
        poses = poses[None, ...]
        trans = data["trans"][None, ...]
    else:
        batch_size = poses.shape[0]
        trans = data["trans"]

    ro = poses[:, :, :9]
    ro_33 = ro.clone().reshape(batch_size, -1, 3, 3)

    origin_ro = ro[:, 0, :].clone().reshape(batch_size, -1, 3, 3)
    origin_ro_inv = origin_ro.transpose(-1, -2).clone()
    origin_ro_inv_expanded = origin_ro_inv.expand(
        batch_size, ro.shape[1], 3, 3).reshape(-1, 3, 3)
    
    aligned_ro_33 = torch.bmm(
        origin_ro_inv_expanded, ro_33.reshape(-1, 3, 3))
    aligned_ro_33 = aligned_ro_33.reshape(batch_size, -1, 3, 3)
    poses[:, :, :9] = aligned_ro_33.reshape(batch_size, -1, 9).detach()
   
    trans = trans-trans[:, 0:1, ...]

    trans = torch.bmm(origin_ro_inv_expanded, trans.view(-1,
                                                         3, 1)).reshape(batch_size, -1, 3).detach()

    data["poses"] = convertPose(
        poses, arepr.MAT, input_repr)
    data["trans"] = trans

    return data
