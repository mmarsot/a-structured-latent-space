# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch

class InvalidShapeException(Exception):
    pass

def checkTensor(*args): 
    for i, arg in enumerate(args):
        if not torch.is_tensor(arg):
            raise TypeError("Expected Tensor, got {} (arg nb {})".format(type(arg),i))

def checkNumDims(*args):
    
    ref_num_dim = len(args[0].shape)

    for arg in args[1:]:
        if(len(arg.shape)!= ref_num_dim):
            raise InvalidShapeException("Tensors should have same number of dims")