# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

from utils.file_management import list_files
from utils.segmentation_utils import *
import os
import tqdm
from data.smpl_sequence import SMPL_Sequence
import argparse
import json
import matplotlib.pyplot as plt
from dtw import accelerated_dtw
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from skimage.feature.peak import peak_local_max
import multiprocessing as mp

def read_mocap(txt_file):

    with open(txt_file) as f:
        lines = f.readlines()

    if "SFU" in txt_file:

        lines = [l.replace('\t\t', '\t').split("\t") for l in lines]

        markers = lines[1]
        data = lines[2:-1]

        data = [d[:-1] for d in data]
        for j, frame in enumerate(data):

            for i, n in enumerate(frame):
                try:
                    data[j][i] = float(n)
                except:

                    data[j][i] = None
        data = np.array(data)
        data = data[:, 1:].reshape(data.shape[0], -1, 3)
    return data

def segment_sequence(seq,
                     actions,
                     actions_data,
                     sub_pose,eps,
                     folder,out_folder,
                     multp,plot):
    
    seq_mocap = None

    if os.path.exists(seq.npz_file.replace('_poses.npz', '.txt')):
        seq_mocap = read_mocap(
            seq.npz_file.replace('_poses.npz', '.txt'))

    seq_data = seq.smpl_data()
    nof = seq_data["poses"].shape[0]

    # if too much frames, only crop at a reduced resolution
    step = int(seq_data["mocap_framerate"] //
                20) if nof > 1000 else 1  # 0.05s

    print("Segmenting %s, %d frames, step=%d" %
            (seq.npz_file, nof, step))
    r = list(range(0, nof, step))
    action_minimas = [[] for a in actions]

    if(nof % step != 0):
        actions_dist_matrix = np.zeros(
            (len(actions), nof//step+1, nof//step+1)) - 1
    else:
        actions_dist_matrix = np.zeros(
            (len(actions), nof//step, nof//step)) - 1

    data = seq_data["poses"]

    # filtered_data = data - np.mean(data, axis=0)

    for k, a_data in enumerate(actions_data):
        if(not multp):
            print("\tAction %d" % k)
        cost_matrix = accelerated_dtw(
            a_data["poses"][:, sub_pose], data[r][:, sub_pose], dist=lambda x, y: np.sum(np.square(x - y)))[1]
        N = a_data["poses"].shape[0]

        for i in r:
            sub_acc_mat = compute_acc_matrix(
                cost_matrix[:, i//step:])

            for j in range(i, seq.number_of_frames(), step):

                actions_dist_matrix[k, i//step, j //
                                    step] = sub_acc_mat[-1, (j-i)//step] / (N+(j-i)//step)
                # not necessary (useful for the plot)
                actions_dist_matrix[k, j//step, i //
                                    step] = actions_dist_matrix[k, i//step, j//step]

        # actions_dist_matrix[k,:,:] / np.sqrt(len(a.sub_pose))
        mini = np.min(-actions_dist_matrix[k, :, :])
        dist_matrix = - actions_dist_matrix[k, :, :] - mini
        if(not multp):
            print("\t\tMinimum at (%d,%d), Value %f" % (*np.unravel_index(np.argmin(actions_dist_matrix[k, :, :]), actions_dist_matrix[k].shape), np.min(
            actions_dist_matrix[k, :, :])))


        minimas = peak_local_max(dist_matrix, min_distance=int(
            seq_data["mocap_framerate"]/6), threshold_abs=(-eps-mini), threshold_rel=configuration["threshold_rel"])

        for m in minimas:
            if(m[0] < m[1]):
                action_minimas[k].append(m)

        action_minimas[k] = remove_overlapping_segments(
            action_minimas[k], dist_matrix)
        if(not multp):
            print("\t\t%d segments selected" % len(action_minimas[k]))
            print("\t\tSegments:", action_minimas[k])

    all_minimas = []
    for k, a in enumerate(actions):
        if(action_minimas[k].size == 0):
            continue
        for m in action_minimas[k]:
            if(not multp):
                print(m[1], m[0])
            all_minimas.append(
                (m, -actions_dist_matrix[k, m[1], m[0]]))

    all_minimas = remove_overlapping_segments_multiple(
        all_minimas) * step
    all_minimas_final = []
    for seg in all_minimas:
        if(seg[1]-seg[0] < 3*seq.framerate() and seg[1]-seg[0] > 0.3*seq.framerate()):
            all_minimas_final.append(seg)
            if(not multp):
                print("\tRemoving overlapping segments and bad cuts...")
                print("\tFinal segments:", all_minimas)

            if(out_folder is not None):
               
                write_segments(seq, all_minimas_final,
                               folder, out_folder, seq_mocap)
               
    
    if not multp and plot:

        X = list(r)
        Y = list(r)
        X, Y = np.meshgrid(X, Y)

        for k, a in enumerate(actions):
            Z = actions_dist_matrix[k, :, :]
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            plt.title(a.npz_file)
            ax.plot_surface(X, Y, Z, cmap=cm.coolwarm)
            if(len(action_minimas[k]) > 0):
                ax.scatter(action_minimas[k][:, 1]*step, action_minimas[k][:, 0]*step, actions_dist_matrix[k][(
                    action_minimas[k][:, 1], action_minimas[k][:, 0])], c='yellow', s=50)

        plt.show()

    
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--outfolder', "-o", default="../../AMASS_cropped/")
    parser.add_argument(
        '--config', default="configurations/cropping/crop_config.json")
    parser.add_argument('--plot', '-p', action="store_true")
    parser.add_argument("--mp",action="store_true",help="multiprocessing")
    args = parser.parse_args()

    configuration = json.load(open(args.config))

    actions = [SMPL_Sequence(f,
                             angle_repr=configuration["reference_angle_repr"],
                             joints=configuration["reference_joints"],
                             pose_hand=configuration["reference_pose_hand"],
                             load_smpl=True,
                             device="cpu") for f in configuration["references"]]

    actions_data = [a.smpl_data() for a in actions]

    sub_pose = []

    for j in configuration["sub_pose"]:
        for i in range(configuration["reference_angle_repr"]["dim"]):
            sub_pose.append(configuration["reference_angle_repr"]["dim"]*j+i)

    amass_folders = os.listdir(configuration["amass_folder"])

    sequence_dict = dict()

    for folder in amass_folders:
        sequence_dict[folder] = []
        files = list_files(os.path.join(configuration["amass_folder"], folder))

        for f in tqdm.tqdm(files):
            if((not '.npz' in f) or 'shape.npz' in f):
                continue

            sequence_dict[folder].append(SMPL_Sequence(f,
                                        angle_repr=configuration["reference_angle_repr"],
                                        joints=configuration["reference_joints"],
                                        pose_hand=configuration["reference_pose_hand"],
                                        load_smpl=False,
                                        device="cpu"))

        if args.mp:
            pool=mp.Pool(mp.cpu_count())
        for seq in sequence_dict[folder]:
            
            if not args.mp:
                segment_sequence(seq,
                                actions,actions_data,
                                sub_pose,
                                configuration["threshold"],
                                folder,args.outfolder,
                                args.mp,args.plot)
            else:
                pool.apply_async(segment_sequence,
                    args=(seq,
                        actions,actions_data,
                        sub_pose,
                        configuration["threshold"],
                        folder,args.outfolder,
                        args.mp,args.plot) 
                )
        if args.mp:
            pool.close()
            pool.join()  
    exit(0)
