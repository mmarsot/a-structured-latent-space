# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

from human_body_prior.body_model.body_model import BodyModel
import torch.nn as nn
import torch

from utils.representation import convertPose
import utils.representation as arepr

SMPLH_JOINTS_BODY = 22
SMPLH_JOINTS_HAND = 30


class SMPLH(nn.Module):

    def __init__(self, configuration):
        """Initialize the body model

        Args:
            configuration (dict): Gives information on the location of the body_model information
            see config_template.json for more details
        """
        super(SMPLH, self).__init__()

        self.num_betas = configuration["body_model"]["num_betas"]
        self.num_dmpls = configuration["body_model"]["num_dmpls"]

        if(self.num_dmpls == 0):
            self.num_dmpls = None

        self.device = configuration["device"]

        self.body_model = BodyModel(bm_path=configuration["body_model"]["smpl_info"],
                                    model_type="smplh",
                                    num_betas=self.num_betas,
                                    num_dmpls=self.num_dmpls,
                                    path_dmpl=configuration["body_model"]["dmpl_info"]).to(self.device)

    def forward(self, data, angle_repr={"name": "aa", "dim": 3}, joints=list(range(SMPLH_JOINTS_BODY)), use_hands=False, all_info=False):
        """Process the batch of smpl data and ouputs a batch of meshes

        Args:
            data ([dict]): dictionnary representing a batch of smpl data
            angle_repr (dict, optional): Angle representation of the input data. Defaults to AA (Axis Angle).
            joints ([list], optional): Subset of joints to consider, missing joint rotation set to I. Defaults to list(range(SMPLH_JOINTS_BODY)).
            use_hands (bool, optional): Whether or not the hand pose is given in the data. Defaults to False.

        Returns:
            [tensor]: The SMPL meshes computed from the input data
        """

        if(len(data["poses"].shape) == 2):  # if no batch
            data2 = dict()

            for k in ["poses", "trans", "betas", "dmpls"]:
                if((self.num_dmpls == 0 or self.num_dmpls == None) and k == "dmpls"):
                    data2["dmpls"] = None
                    continue
                data2[k] = data[k].reshape(1, *data[k].shape)

        elif(len(data["poses"].shape) == 1):  # if single frame and no batch
            data2 = dict()

            for k in ["poses", "trans", "dmpls"]:
                if((self.num_dmpls == 0 or self.num_dmpls == None) and k == "dmpls"):
                    data2["dmpls"] = None
                    continue

                data2[k] = data[k].reshape(1, 1, *data[k].shape)
            data2["betas"] = data["betas"].reshape(1, *data["betas"].shape)

        else:
            data2 = data

        batch_size = data2["poses"].shape[0]
        nof = data2["poses"].shape[1]

        poses = data2["poses"].float().reshape(
            batch_size*nof, -1)
       
        if(angle_repr["name"] != "aa"):
           
            poses = convertPose(poses, angle_repr, {"name": "aa", "dim": 3})
       
        if(use_hands == False):
            pose_hand = torch.zeros(
                (poses.shape[0], SMPLH_JOINTS_HAND*3), dtype=torch.float32, device=self.device)
        else:
            pose_hand = poses[:, SMPLH_JOINTS_BODY*3:]

        # only consider the subset of joints
        full_body_pose = torch.zeros(
            (poses.shape[0], SMPLH_JOINTS_BODY*3), dtype=torch.float32, device=self.device)
        
        for i, j in enumerate(joints):
            
            full_body_pose[:, j*3:(j+1)*3] = poses[:, i*3:(i+1)*3]

        if (self.num_dmpls is not None):
            data2["dmpls"] = data2["dmpls"].float().reshape(
                batch_size*nof, -1)[:, :self.num_dmpls]
       
        model = self.body_model(
            root_orient=full_body_pose[:, 0:3],
            pose_body=full_body_pose[:, 3:SMPLH_JOINTS_BODY*3],
            pose_hand=pose_hand,
            betas=data2["betas"].repeat_interleave(
                nof, 0).float()[:, :self.num_betas],
            trans=data2["trans"].float().reshape(
                batch_size*nof, -1),
            dmpls=data2["dmpls"])

        if(all_info):
            return model
        return model.v.reshape(batch_size, nof, 6890, 3).squeeze()

    def get_faces(self):
        return self.body_model.f

    def to(self, device):
        self.device = device
        self.body_model = self.body_model.to(device)
        return self
