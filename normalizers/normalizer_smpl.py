# Author : Mathieu Marsot 
# Release date : September 2022
# This file is released under the agreement of the license.pdf file from the git repository,
# please refer to it for more details
# If you use this code in a research publication you agree to cite the following:
# M. Marsot, S. Wuhrer, J.-S. Franco, S. Durocher, A Structured Latent Space For Human Body Motion Generation, International Conference on 3D Vision 2022,
# https://arxiv.org/abs/2106.04387

import torch


class SMPL_normalizer:

    def __init__(self, configuration):
        """
        Args:
            configuration (dict): configuration parameters for the normalization
            see config_template.json for more details
        """

        self.normalize_poses = configuration["normalization"]["poses"]
        self.normalize_trans = configuration["normalization"]["trans"]
        self.normalize_timestamps = configuration["normalization"]["timestamps"]
        self.normalize_betas = configuration["normalization"]["betas"]
        self.normalize_dmpls = configuration["normalization"]["dmpls"]

        self.only_center = configuration["normalization"]["only_center"]
        self.stats = torch.load(
            configuration["normalization"]["normalization_file"])

        self.dtype = configuration["dtype"]
        self.device = configuration["device"]

        for key in self.stats.keys():
            self.stats[key] = (self.stats[key][0].type(self.dtype).to(
                self.device).detach(), self.stats[key][1].type(self.dtype).to(self.device).detach())

    def normalize(self, data):
        """
        Args:
            data (dict): SMPL input data

        Returns:
            dict: the normalized SMPL data
        """

        normalized_data = dict(data)
        if(self.normalize_poses):
            normalized_data["poses"] = (
                normalized_data["poses"] - self.stats["poses"][0][:normalized_data["poses"].shape[-1]]) / self.stats["poses"][1][:normalized_data["poses"].shape[-1]]

        if self.normalize_dmpls:
            normalized_data["dmpls"] = (
                normalized_data["dmpls"] - self.stats["dmpls"][0]) / self.stats["dmpls"][1]

        if(self.normalize_betas):
            n_betas = normalized_data["betas"].shape[-1]
            normalized_data["betas"] = (
                normalized_data["betas"] - self.stats["betas"][0][:n_betas]) / self.stats["betas"][1][:n_betas]

        if(self.normalize_trans):
            if(self.only_center):
                normalized_data["trans"] = normalized_data["trans"] - \
                    self.stats["trans"][0]
            else:
                trans_std = (
                    normalized_data["trans"] - self.stats["trans"][0]) / (self.stats["trans"][1] - self.stats["trans"][0])
                normalized_data["trans"] = trans_std * (1-(-1)) + (-1)

        if(self.normalize_timestamps):
            if(self.only_center):
                normalized_data["timestamp"] = normalized_data["timestamp"] - \
                    self.stats["timestamp"][0]
            else:
                timestamp_std = (
                    normalized_data["timestamp"] - self.stats["timestamp"][0]) / (self.stats["timestamp"][1] - self.stats["timestamp"][0])
                normalized_data["timestamp"] = timestamp_std * (1-0) + 0

        return normalized_data

    def unnormalize(self, data):
        """
        Args:
            data (dict): SMPL input data

        Returns:
            dict: the normalized SMPL data
        """

        unnormalized_data = dict(data)
        if(self.normalize_poses):
            unnormalized_data["poses"] = unnormalized_data["poses"] * \
                self.stats["poses"][1][:unnormalized_data["poses"].shape[-1]
                                       ] + self.stats["poses"][0][:unnormalized_data["poses"].shape[-1]]

        if self.normalize_dmpls:
            unnormalized_data["dmpls"] = unnormalized_data["dmpls"] * \
                self.stats["dmpls"][1] + self.stats["dmpls"][0]
        if(self.normalize_betas):
            n_betas = unnormalized_data["betas"].shape[-1]
            unnormalized_data["betas"] = unnormalized_data["betas"] * \
                self.stats["betas"][1][:n_betas] + \
                self.stats["betas"][0][:n_betas]

        if(self.normalize_trans):
            unnormalized_data["trans"] = ((unnormalized_data["trans"]+1)/2) * (
                self.stats["trans"][1] - self.stats["trans"][0]) + self.stats["trans"][0]

        if(self.normalize_timestamps):
            unnormalized_data["timestamp"] = ((unnormalized_data["timestamp"]+0)/(1-0)) * (
                self.stats["timestamp"][1] - self.stats["timestamp"][0]) + self.stats["timestamp"][0]

        return unnormalized_data

    def normalize_timestamps_(self, timestamps):

        timestamp_std = (
            timestamps - self.stats["timestamp"][0]) / (self.stats["timestamp"][1] - self.stats["timestamp"][0])
        timestamps = timestamp_std

        return timestamps
